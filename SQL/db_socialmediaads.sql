-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 01:19 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_socialmediaads`
--

-- --------------------------------------------------------

--
-- Table structure for table `vc_accesslevel`
--

CREATE TABLE `vc_accesslevel` (
  `id` int(11) NOT NULL,
  `label_id` tinyint(11) DEFAULT NULL,
  `niech_id` tinyint(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_accesslevel`
--

INSERT INTO `vc_accesslevel` (`id`, `label_id`, `niech_id`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 3, 1, 1, 0, '2019-09-12 09:10:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vc_admin`
--

CREATE TABLE `vc_admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `username` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `password` varchar(256) NOT NULL,
  `booking_pass` varchar(256) NOT NULL,
  `profile_pic` varchar(256) NOT NULL,
  `last_login` varchar(256) NOT NULL,
  `status` int(11) NOT NULL COMMENT '2:admin,1:staff'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_admin`
--

INSERT INTO `vc_admin` (`id`, `first_name`, `last_name`, `email`, `username`, `permission`, `password`, `booking_pass`, `profile_pic`, `last_login`, `status`) VALUES
(1, 'script', 'creator', 'info@fabricimageeditor.com', 'sc-admin', '', 'e71029974e376d454aabf56aaae54b12', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '2019-06-10 10:14:32', 2);

-- --------------------------------------------------------

--
-- Table structure for table `vc_audio_library`
--

CREATE TABLE `vc_audio_library` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `music_name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_audio_library`
--

INSERT INTO `vc_audio_library` (`id`, `user_id`, `music_name`, `type`, `is_active`, `is_delete`, `created_date`) VALUES
(1, 0, 'kanha.mp3', 'Classic', 1, 0, '2019-09-12');

-- --------------------------------------------------------

--
-- Table structure for table `vc_category`
--

CREATE TABLE `vc_category` (
  `id` int(11) NOT NULL,
  `cat_title` varchar(200) DEFAULT NULL,
  `cat_url` varchar(245) DEFAULT NULL,
  `cat_img` varchar(245) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_category`
--

INSERT INTO `vc_category` (`id`, `cat_title`, `cat_url`, `cat_img`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'Logo Design', 'http://localhost/3D-logo-reveals', 'Skype_logo.png', 1, 0, '2019-08-07 15:00:35', '2019-08-07 16:07:17'),
(2, 'Socialtours', 'http://54.175.94.102/videocreationapp/users', 'Ink_splatter.png', 1, 0, '2019-08-23 06:35:31', '2019-08-23 10:05:31'),
(3, NULL, NULL, NULL, 1, 0, '2019-08-23 16:44:09', '2019-08-23 20:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `vc_level`
--

CREATE TABLE `vc_level` (
  `id` int(11) NOT NULL,
  `label_name` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_level`
--

INSERT INTO `vc_level` (`id`, `label_name`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'Daimond', 1, 0, '2019-09-07 13:18:23', '0000-00-00 00:00:00'),
(2, 'Gold', 1, 0, '2019-09-07 13:18:35', '0000-00-00 00:00:00'),
(3, 'Silver', 1, 0, '2019-09-12 09:10:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_project`
--

CREATE TABLE `vc_project` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `tempid` int(11) DEFAULT NULL,
  `uvidname` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vc_setting`
--

CREATE TABLE `vc_setting` (
  `id` int(11) NOT NULL,
  `parameter` varchar(250) DEFAULT NULL,
  `value` text,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_setting`
--

INSERT INTO `vc_setting` (`id`, `parameter`, `value`, `is_delete`, `created_date`) VALUES
(1, 'Copyright', 'Social Ads . All rights reserved  .', 0, '2019-09-12 16:14:23'),
(2, 'Email Id', 'socialads@gmail.com', 0, '2019-09-12 16:14:23'),
(3, 'Phone No', '9438432291', 0, '2019-09-12 16:14:23'),
(4, 'Address', 'Converthink Solutions Private Limited', 0, '2019-09-12 16:17:29'),
(5, 'Facebook', 'https://www.facebook.com/', 0, '2019-09-12 16:18:23'),
(6, 'Twitter', 'https://twitter.com/', 0, '2019-09-12 16:18:23'),
(7, 'Linkedin', 'https://www.linkedin.com/', 0, '2019-09-12 16:22:05'),
(8, 'Pinterest', 'https://www.pinterest.com/', 0, '2019-09-12 16:22:05'),
(9, 'Project Team', 'Socialads Team', 0, '2019-09-13 19:15:28');

-- --------------------------------------------------------

--
-- Table structure for table `vc_support`
--

CREATE TABLE `vc_support` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_support`
--

INSERT INTO `vc_support` (`id`, `title`, `description`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'Can You Help Me ?', 'Yes, Please contact with admin following details.', 1, 0, '2019-09-12 09:10:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_templatescene`
--

CREATE TABLE `vc_templatescene` (
  `id` int(11) NOT NULL,
  `temp_id` int(11) DEFAULT NULL,
  `scenename` varchar(250) DEFAULT NULL,
  `videoname` varchar(250) DEFAULT NULL,
  `vthumb` varchar(250) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_templatescene`
--

INSERT INTO `vc_templatescene` (`id`, `temp_id`, `scenename`, `videoname`, `vthumb`, `is_active`, `is_delete`, `created_date`) VALUES
(6, 1, 'Scene 1', NULL, 'Chrysanthemum1.jpg', 1, 0, '2019-09-19 11:56:53'),
(7, 1, 'Scene 2', NULL, 'Desert.jpg', 1, 0, '2019-09-19 11:57:15');

-- --------------------------------------------------------

--
-- Table structure for table `vc_training`
--

CREATE TABLE `vc_training` (
  `id` int(11) NOT NULL,
  `tquestion` text,
  `tans` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_training`
--

INSERT INTO `vc_training` (`id`, `tquestion`, `tans`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'What is training ?', 'We will give you training for video creation', 1, 0, '2019-09-12 09:09:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_user`
--

CREATE TABLE `vc_user` (
  `id` int(11) NOT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `user_email` varchar(200) DEFAULT NULL,
  `user_phone` varchar(100) DEFAULT NULL,
  `user_address` varchar(250) DEFAULT NULL,
  `user_pass` varchar(200) DEFAULT NULL,
  `profile_photo` varchar(200) DEFAULT NULL,
  `item_number` varchar(100) DEFAULT NULL,
  `product_id` varchar(100) DEFAULT NULL,
  `label_id` varchar(100) DEFAULT NULL,
  `removal_id` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_user`
--

INSERT INTO `vc_user` (`id`, `fname`, `user_email`, `user_phone`, `user_address`, `user_pass`, `profile_photo`, `item_number`, `product_id`, `label_id`, `removal_id`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'chitaranjan samantaray', 'chitaranjan100@gmail.com', '9438432291', 'Bhubaneswar', '202cb962ac59075b964b07152d234b70', NULL, '', '', '2,1,4,3', '', 1, 0, '2019-08-30 00:00:00', '2019-08-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_videolayer`
--

CREATE TABLE `vc_videolayer` (
  `id` int(11) NOT NULL,
  `tempid` int(11) DEFAULT NULL,
  `layername` varchar(200) DEFAULT NULL,
  `layeraudio` varchar(245) DEFAULT NULL,
  `layervideo` varchar(245) DEFAULT NULL,
  `layertext` varchar(245) DEFAULT NULL,
  `maxlength_text` varchar(100) DEFAULT NULL,
  `layer_image` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vc_videotemplate`
--

CREATE TABLE `vc_videotemplate` (
  `id` int(11) NOT NULL,
  `tname` varchar(200) DEFAULT NULL,
  `tfname` varchar(250) DEFAULT NULL,
  `tfvideo` varchar(200) DEFAULT NULL,
  `thumbnailname` varchar(250) DEFAULT NULL,
  `jsonfile` varchar(100) DEFAULT NULL,
  `nooflayer` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_videotemplate`
--

INSERT INTO `vc_videotemplate` (`id`, `tname`, `tfname`, `tfvideo`, `thumbnailname`, `jsonfile`, `nooflayer`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'Travel', 'Travel.aep', NULL, 'travel.png', 'ctsagain.json', '5', 1, 0, '2019-09-12 09:07:41', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vc_accesslevel`
--
ALTER TABLE `vc_accesslevel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_admin`
--
ALTER TABLE `vc_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_audio_library`
--
ALTER TABLE `vc_audio_library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_category`
--
ALTER TABLE `vc_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_level`
--
ALTER TABLE `vc_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_project`
--
ALTER TABLE `vc_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_setting`
--
ALTER TABLE `vc_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_support`
--
ALTER TABLE `vc_support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_templatescene`
--
ALTER TABLE `vc_templatescene`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_training`
--
ALTER TABLE `vc_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_user`
--
ALTER TABLE `vc_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_videolayer`
--
ALTER TABLE `vc_videolayer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_videotemplate`
--
ALTER TABLE `vc_videotemplate`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vc_accesslevel`
--
ALTER TABLE `vc_accesslevel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_admin`
--
ALTER TABLE `vc_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_audio_library`
--
ALTER TABLE `vc_audio_library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_category`
--
ALTER TABLE `vc_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vc_level`
--
ALTER TABLE `vc_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vc_project`
--
ALTER TABLE `vc_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vc_setting`
--
ALTER TABLE `vc_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vc_support`
--
ALTER TABLE `vc_support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_templatescene`
--
ALTER TABLE `vc_templatescene`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vc_training`
--
ALTER TABLE `vc_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_user`
--
ALTER TABLE `vc_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_videolayer`
--
ALTER TABLE `vc_videolayer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vc_videotemplate`
--
ALTER TABLE `vc_videotemplate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
