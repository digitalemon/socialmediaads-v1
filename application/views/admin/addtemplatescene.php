        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>Add Scene</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php echo base_url('admin/users/inserttemplatescene');?>" method="post" name="frmprofile" enctype="multipart/form-data">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Select Template Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<select class="form-control" name="template_id" id="template_id" required="required" <?php if(isset($edit_psdfile)){ ?>Disabled<?php } ?>>
																							  <option value="">Select Template Name</option>
																							  <?php 
																							  foreach($templates as $value) { ?>
																							  <option value="<?php echo $value->id; ?>" <?php if($value->id == $edit_psdfile->temp_id){ ?>selected="selected" <?php } ?>><?php echo $value->tname; ?></option>
																							  <?php } ?>
																							</select>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Scene Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="text" class="form-control" name="scenename" id="scenename" value="" placeholder="Scenename">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Video file</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="file" class="form-control" name="videoname" id="videoname" placeholder="Enter Video File">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Video Thumbnail</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="file" class="form-control" name="vthumb" id="vthumb" value="" placeholder="Enter Video Thumbnail!">
																						</div>
																					</div>
																					</div>
																					<div class="form-actions mt-10">			
																						<button type="submit" class="btn btn-success mr-10 mb-30">Add Scene</button>
																					</div>				
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>