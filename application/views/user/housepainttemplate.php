<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Social Ads</title>
	<meta name="description" content="Social Ads is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Video Creator Admin, Elmeradmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<!-- Data table CSS -->
	<link href="<?php echo base_url();?>media/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url();?>media/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
	<!-- Custom CSS -->
	<link href="<?php echo base_url();?>media/dist/css/style.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>media/assets/css/slick.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>media/assets/css/slick-theme.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>media/assets/css/style1.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>media/assets/css/mkhplayer.default.css" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" href="<?php echo base_url();?>media/assets/css/spectrum.css">
	<link rel="stylesheet" href="<?php echo base_url();?>media/assets/css/cropper.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>media/assets/css/editor.css">
	<link href="<?php echo base_url();?>media/assets/css/fabric/prism.css" rel="stylesheet">
	<script src="<?php echo base_url();?>media/assets/js/fabric/jquery.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/fabric/prism.js"></script>
	<script>
	  (function() {
		var fabricUrl = '<?php echo base_url();?>media/assets/js/fabric/fabric.js';
		if (document.location.search.indexOf('load_fabric_from=') > -1) {
		  var match = document.location.search.match(/load_fabric_from=([^&]*)/);
		  if (match && match[1]) {
			fabricUrl = match[1];
		  }
		}
		document.write('<script src="' + fabricUrl + '"><\/script>');
	  })();
	</script>
	<script src="<?php echo base_url();?>media/assets/js/fabric/angularjs/1.2.6/angular.min.js"></script>
	<script async defer src="<?php echo base_url();?>media/assets/js/fabric/buttons/buttons.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/fabric/paster.js"></script>
</head>

<body>
    <div class="wrapper theme-1-active pimary-color-green">
        <!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top pl-15">
			<div class="mobile-only-brand pull-left">
				<!--<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="<?php echo base_url('users')?>">
							<img class="brand-img" src="<?php echo base_url();?>media/dist/img/logo.png" alt="brand">
							<span class="brand-text" style="font-size:18px;">Socialmediaoutros</span>
						</a>
					</div>
				</div>-->
				<div style="display: inline-flex; padding-top: 7px; padding-bottom: 7px;">
						<div class="videoduration">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							<span class="vd-label">Duration :</span>
							<span>00:30 Sec</span>
						</div>
						<div class="videoduration">
							<i class="fa fa-picture-o" aria-hidden="true"></i>
							<span class="vd-label">Scenes :</span>
							<span>1</span>
						</div>
				</div>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
			</div>	
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">
					<div style="padding-top: 7px; padding-bottom: 7px;">
					<div class="videoduration">
						<i class="zmdi zmdi-landscape mr-10" aria-hidden="true"></i>
						<a href="<?php echo base_url('users')?>"><span class="vd-label">Back to Dashboard</span></a>
					</div>
					<div class="videoduration">
						<i class="zmdi zmdi-headset-mic mr-10" aria-hidden="true"></i>
						<span class="vd-label">Support</span>
					</div>
					</div>
				</ul>
			</div>
		</nav>		
		<!-- /Top Menu Items -->
		
		<!-- Main Content -->
		<form action="<?php echo base_url('users/projectshp');?>" method="post" onsubmit="return check_project();"  name="frmpmusic" id="frmpmusic" enctype="multipart/form-data">
		<div id="remove-bg" class="page-wrapper ml-0 banner-style pb-0">
                <div class="shapes">
                    <svg class="abstract-svg-1" viewBox="0 0 102 102">
                        <circle cx="50" cy="50" r="50"></circle>
                    </svg>

                    <svg class="abstract-svg-2" viewBox="0 0 438.536 438.536">

                        <path d="M414.41,24.123C398.333,8.042,378.963,0,356.315,0H82.228C59.58,0,40.21,8.042,24.126,24.123
   C8.045,40.207,0.003,59.576,0.003,82.225v274.084c0,22.647,8.042,42.018,24.123,58.102c16.084,16.084,35.454,24.126,58.102,24.126
   h274.084c22.648,0,42.018-8.042,58.095-24.126c16.084-16.084,24.126-35.454,24.126-58.102V82.225
   C438.532,59.576,430.49,40.204,414.41,24.123z" />

                    </svg>

                    <svg class="abstract-svg-3" viewBox="0 0 401.998 401.998">

                        <path d="M377.87,24.126C361.786,8.042,342.417,0,319.769,0H82.227C59.579,0,40.211,8.042,24.125,24.126
   C8.044,40.212,0.002,59.576,0.002,82.228v237.543c0,22.647,8.042,42.014,24.123,58.101c16.086,16.085,35.454,24.127,58.102,24.127
   h237.542c22.648,0,42.011-8.042,58.102-24.127c16.085-16.087,24.126-35.453,24.126-58.101V82.228
   C401.993,59.58,393.951,40.212,377.87,24.126z M365.448,319.771c0,12.559-4.47,23.314-13.415,32.264
   c-8.945,8.945-19.698,13.411-32.265,13.411H82.227c-12.563,0-23.317-4.466-32.264-13.411c-8.945-8.949-13.418-19.705-13.418-32.264
   V82.228c0-12.562,4.473-23.316,13.418-32.264c8.947-8.946,19.701-13.418,32.264-13.418h237.542
   c12.566,0,23.319,4.473,32.265,13.418c8.945,8.947,13.415,19.701,13.415,32.264V319.771L365.448,319.771z" />

                    </svg>

                    <svg class="abstract-svg-4" viewBox="0 0 401.998 401.998">
                        <path d="M377.87,24.126C361.786,8.042,342.417,0,319.769,0H82.227C59.579,0,40.211,8.042,24.125,24.126
   C8.044,40.212,0.002,59.576,0.002,82.228v237.543c0,22.647,8.042,42.014,24.123,58.101c16.086,16.085,35.454,24.127,58.102,24.127
   h237.542c22.648,0,42.011-8.042,58.102-24.127c16.085-16.087,24.126-35.453,24.126-58.101V82.228
   C401.993,59.58,393.951,40.212,377.87,24.126z M365.448,319.771c0,12.559-4.47,23.314-13.415,32.264
   c-8.945,8.945-19.698,13.411-32.265,13.411H82.227c-12.563,0-23.317-4.466-32.264-13.411c-8.945-8.949-13.418-19.705-13.418-32.264
   V82.228c0-12.562,4.473-23.316,13.418-32.264c8.947-8.946,19.701-13.418,32.264-13.418h237.542
   c12.566,0,23.319,4.473,32.265,13.418c8.945,8.947,13.415,19.701,13.415,32.264V319.771L365.448,319.771z" />
                    </svg>                  
					<svg class="abstract-svg-5" viewBox="0 0 184.58 184.58">

                        <path d="M182.004,146.234L108.745,19.345c-3.435-5.949-9.586-9.5-16.455-9.5s-13.021,3.551-16.455,9.5L2.576,146.234
  c-3.435,5.948-3.435,13.051,0,19c3.435,5.949,9.586,9.5,16.455,9.5h146.518c6.869,0,13.021-3.552,16.455-9.5
  C185.438,159.285,185.438,152.182,182.004,146.234z M169.88,158.234c-0.435,0.751-1.725,2.5-4.331,2.5H19.031
  c-2.606,0-3.896-1.749-4.331-2.5c-0.434-0.752-1.302-2.744,0.001-5L87.96,26.345c1.303-2.256,3.462-2.5,4.33-2.5
  s3.027,0.244,4.33,2.5l73.259,126.889C171.181,155.49,170.313,157.482,169.88,158.234z" />

                    </svg>

                </div>
        <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<!-- Tab Menu Start -->
					<div class="col-md-8 col-md-offset-2 col-sm-12">
						<div class="tab_menu_list">
							<ul class="nav nav-pills">
								<li class="nav-item active" id="edit_tab"><a class="nav-link" href="javascript:void(0);" onclick="next_step('edit');" >
								<span class="tb-icon"><i class="fa fa-pencil" aria-hidden="true"></i></span> <span class="tb-text">Edit</span></a></li>
								<li class="nav-item" id="music_tab"><a class="nav-link" href="javascript:void(0);" onclick="next_step('music');">
								<span class="tb-icon"><i class="fa fa-music" aria-hidden="true"></i></span> <span class="tb-text">Music</span></a></li>
								<li class="nav-item" id="preview_tab"><a class="nav-link" href="javascript:void(0);" onclick="next_step('preview');" >
								<span class="tb-icon"><i class="fa fa-play" aria-hidden="true"></i></span> <span class="tb-text">Render</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /Tab Menu End -->
				</div>
				<!-- /Title -->			
				<div class="">					
					<div class="">					
						<div class="tab-content">
							<div class="tab-pane fade active in" id="edit" role="tabpanel">
								<div class="resizeables-parent">
									<div class="rf-edit-screen-container">
										<div class="rf-container">
											<div class="side-1">
												<div class="left-side-box">
													<!--<div class="box-header"></div>-->
													<div class="box-body screen-areas">
														<div class="content-scroll">
															<div class="rf-areas-category rf-area-text">
																<div class="rf-areas-container">
																	<span class="rf-areas-container-text span">Add Text 1</span>
																</div>
																<div class="text-area-handler area-handler">
																	<div class="area-container">
																		<div class="rf-advanced-input-container">
																			<div class="textarea-container">
																				<input placeholder="layer text 1" type='text' name="textname1" id="textname1" onkeyup="addtitle('textname1','textname_length1','10','2');" maxlength="10" class="advanced-textarea">
																			</div>
																			<div class="hint-container">
																				<span class="hint_text" id="textname_length1">0 / 10 characters</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="rf-areas-category rf-area-text">
																<div class="rf-areas-container">
																	<span class="rf-areas-container-text span">Add Text 2</span>
																</div>
																<div class="text-area-handler area-handler">
																	<div class="area-container">
																		<div class="rf-advanced-input-container">
																			<div class="textarea-container">
																				<input placeholder="layer text 2" type='text' name="textname2" id="textname2" onkeyup="addtitle1('textname2','textname_length2','5','2');" maxlength="5" class="advanced-textarea">
																			</div>
																			<div class="hint-container">
																				<span class="hint_text" id="textname_length2">0 / 5 characters</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="rf-areas-category rf-area-text">
																<div class="rf-areas-container">
																	<span class="rf-areas-container-text span">Add Text 3</span>
																</div>
																<div class="text-area-handler area-handler">
																	<div class="area-container">
																		<div class="rf-advanced-input-container">
																			<div class="textarea-container">
																				<input placeholder="layer text 3" type='text' name="textname3" id="textname3" onkeyup="addtitle2('textname3','textname_length3','5','3');" maxlength="5" class="advanced-textarea">
																			</div>
																			<div class="hint-container">
																				<span class="hint_text" id="textname_length3">0 / 5 characters</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="rf-areas-category rf-area-text">
																<div class="rf-areas-container">
																	<span class="rf-areas-container-text span">Add Description</span>
																</div>
																<div class="text-area-handler area-handler">
																	<div class="area-container">
																		<div class="rf-advanced-input-container">
																			<div class="textarea-container">
																				<textarea placeholder="layer text 1" type='text' name="desc1" id="desc1" onkeyup="Adddescription('desc1','desc_length1','40','1');" class="advanced-textarea" maxlength="40" value=""></textarea>
																			</div>
																			<div class="hint-container">
																				<span class="hint_text" id="desc_length1">0 / 10 characters</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<span class="checklayer" id="layer_text1">
																<div class="rf-areas-category rf-area-text">
																	<div class="rf-areas-container">
																		<span class="rf-areas-container-text span">Add Bg Image 1</span>
																	</div>
																	<div class="text-area-handler area-handler">
																		<div class="area-container">
																			<div class="rf-advanced-input-container">
																				<div class="textarea-container">
																				<input type='file' name="addimage1" id="addimage1" class="advanced-textarea">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</span>
															<span class="checklayer d-none" id="layer_text2">
																<div class="rf-areas-category rf-area-text">
																	<div class="rf-areas-container">
																		<span class="rf-areas-container-text span">Add Bg2 Image 1</span>
																	</div>
																	<div class="text-area-handler area-handler">
																		<div class="area-container">
																			<div class="rf-advanced-input-container">
																				<div class="textarea-container">
																					<input type='file' name="addimage2" id="addimage2" class="advanced-textarea">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</span>
														</div>
													</div>
													<div class="box-footer text-center btn-action">
														<a href="javascript:void(0);" onclick="previous_step('1');" class="btn btn-default"><i class="fa fa-angle-left"></i> Previous </a>
														<a href="javascript:void(0);" onclick="next_step('2');"  id="next_tab_music" class="btn btn-white">Next Step <i class="fa fa-angle-right"></i></a>
														<a href="javascript:void(0);" onclick="next_step('music');"  id="next_music" class="btn btn-white" style="display: none;">Next Step <i class="fa fa-angle-right"></i></a>
													</div>
												</div>
											</div>				
											<div class="side-2">
												<div class="side-2-preview">
													<div class="slider-for mb-0">
														<div class="item">
															<div id="div_text1" class="absolute-text-value" style="color: rgb(255, 255, 255); top: 15%;">
																<div class="wrapping-div-content ">
																	<h1 class="text-area-value h1" id="select_text1"></h1>
																</div>
															</div>
															<div id="div_desc1" class="absolute-text-value" style="color: rgb(255, 255, 255); bottom: 30%;">
																<div class="wrapping-div-content ">
																	<span class="text-area-value" id="desc_text1"></span>
																</div>
															</div>
															<div id="div_text2" class="absolute-text-value" style="color: rgb(255, 255, 255); top: 15%;">
																<div class="wrapping-div-content ">
																	<h1 class="text-area-value h1" id="select_text2"></h1>
																</div>
															</div>
															<div id="div_desc2" class="absolute-text-value" style="color: rgb(255, 255, 255); bottom: 30%;">
																<div class="wrapping-div-content ">
																	<span class="text-area-value" id="desc_text2"></span>
																</div>
															</div>
															<!--<img id="output1" src="<?php echo base_url();?>media/uploads/videothumb/Jellyfish.png" class="img-responsive">-->
															<canvas id="canvas" width="744px" height="420px" class="img-responsive" style="border: 1px solid #D0CECE;"></canvas>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<footer class="footer" style="background-color: #eef5ff;min-height: 130px;height: 130px;margin-left: 5px;z-index: 10;">
									<div class="screens-container" style="padding: 0px;">
										<div class="sortable-list slider-nav" style="margin-bottom:0px;">
											<div class="flex-list item-slide" id="current1">
												<div class="screenspreview_thumbnail" id="active1">
													<div class="videopreview">
														<div class="screens_effect" id="1" onclick="addbgimage('<?php echo base_url();?>media/uploads/videothumb/house.png','1'),get_id('1');"></div>
														<input class="img_scene" type="hidden" name="img_scene" id="img_scene" value="1">
														<img id="img1" src="<?php echo base_url();?>media/uploads/videothumb/house.png" alt="">
														<div class="screen-number">1</div>	
													</div>
												</div>
											</div>
											<div class="flex-list item-slide" id="current2">
												<div class="screenspreview_thumbnail" id="active2">
													<div class="videopreview">
														<div class="screens_effect" id="2" onclick="addbgimage('<?php echo base_url();?>media/uploads/videothumb/house.png','2'),get_id('2');"></div>
														<input class="img_scene" type="hidden" name="img_scene" id="img_scene" value="2">
														<img id="img2" src="<?php echo base_url();?>media/uploads/videothumb/house.png" alt="">
														<div class="screen-number">2</div>
															
													</div>
												</div>
											</div>
										</div>
									</div>
								</footer>
							</div>
							<input type='hidden' name="music_name_val" id="music_name_val" value="" class="advanced-textarea">
							<div class="tab-pane fade" id="music" role="tabpanel">
								<div class="container pt-25">
									<div class="row">
										<div class="music-body">
											<div class="col-md-12">
											<div class="col-md-4" style="font-size: 18px;">
												<span class="animated-list span" id="replace_music">
											</div>
											<div class="col-md-8">
												<div class="col-md-1">
													<div id="playPause" style="color:#ff1744;font-size:2rem;cursor: pointer;">
														<span id="play" style="display: none;"><i class="icon-control-play"></i></span>
														<span id="pause" style="display: none;"><i class="icon-control-pause"></i></span>
													</div>	
												</div>
												<div class="col-md-11">
													<div id="waveform"></div>										
												</div>
											</div>
												<table id="example" class="table table-striped dt-responsive nowrap music-table" style="width:100%">
													<thead>
														<tr>
															<th>Name</th>
															<th>Genres</th>
															<th>Music Preview</th>
															<th class="no-sort">Add</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i=0;
														foreach($audioplayer as $audiovalue) {
														$i++;
														?>
														<tr id="playlist">
															<td><?php echo $audiovalue->music_name; ?></td>
															<td><?php echo $audiovalue->type; ?></td>
															<td>
																<div class="wavebar" data-path="<?php echo base_url().'media/uploads/audio_files/'.$audiovalue->music_name;?>" id="audio_section" >
																	<div class="col-lg-12">
																		<div class="col-md-2">
																			<div class="btn-play-pause">
																				<span><i class="icon-control-play"></i>/<i class="icon-control-pause"></i></span>	
																			</div>
																		</div>
																		<div class="col-md-10">
																			<div class="waveform__counter"></div>
																			<div class="wave-container"></div>
																			<div class="waveform__duration"></div>
																		</div>															
																	</div>
																</div>
																<!--<audio style="width: 100%;" controls>
																  <source src="<?php echo base_url().'media/uploads/audio_files/'.$audiovalue->music_name;?>" id="audio_section" type="audio/mpeg">
																</audio>-->
															</td>
															<td>
																<div class="video-audio-track-add">
																<a class="audio-add-btn" onclick="add_music('<?=$audiovalue->music_name;?>','<?=$audiovalue->type;?>');" href="<?php echo base_url().'media/uploads/audio_files/'.$audiovalue->music_name;?>"><i color="white" class="icon-control-play"></i></a>
																</div>
															</td>
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="preview" role="tabpanel">
								<div class="container pt-25">
									<div class="row">
										<div class="col-lg-6 col-lg-offset-3 pt-50">
											<div class="panel-body">
												<h3 class="text-center mb-4 txt-dark"><strong>Render Your Project</strong></h3>
												<p class="text-center mb-4 txt-dark">You are just one step away from creating an awesome outro video. Click on the “Create my video” button below and let the magic happen.</p>
												<div class="text-center" style="margin-top:40px;">
													<input type="hidden" name="tid" value="<?php echo $this->uri->segment(3); ?>">
													<button type="button" class="btn btn-primary mr-10 mb-30 bg-color equal-font" data-toggle="modal" data-target="#video-render-modal" >Create My Video</button>
													<!--<button type="submit" onclick="prgressBar(0);"class="btn btn-primary mr-10 mb-30" data-toggle="modal" data-target="video-render-modal">Render Video</button>--> 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal modal-bg fade" id="preview_submit" tabindex="-1" role="dialog" data-backdrop="static">
								<div class="innerpage-decor">
									<div class="innerpage-circle1">
										<img src="<?php echo base_url();?>media/assets/images/circle1.png" alt="">
									</div>
									<div class="innerpage-circle2">
										<img src="<?php echo base_url();?>media/assets/images/circle2.png" alt="">
									</div>
									<div class="innerpage-circle3">
										<img src="<?php echo base_url();?>media/assets/images/circle3.png" alt="">
									</div>
								</div>
								<div class="modal-dialog  modal-dialog-centered" role="document" style="max-width:768px;width:768px;">
									<div class="modal-content">
										<div class="modal-body" style="min-height:415px;padding: 1px;">
											<div class="col-lg-12" style="padding: 0;margin:auto;">
												<div id="ember1160" class="VideoPreviewRender ember-view"> 
													<div class="centered">
														<div class="bubble"></div>
														<div class="bubble"></div>
														<div class="bubble"></div>
														<div class="bubble"></div>
													</div>
													<div id="ember1161" class="VideoPreviewRendering ember-view"><div class="VideoPreviewRendering__Headings">
														<span class="is-current" style="width: 100%;"></span>
														</div>
														<div style="padding:5px;">
														  <div class="percentage" id="precent"></div>
														  <div class="loader">
															<div class="trackbar">
															  <div class="loadbar"></div>
															</div>
															<div class="glow"></div>
														  </div>
														</div>
													</div>
													<div class="bar">
														<div class="circle"></div>
														<p>Rendering...</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="success_message" tabindex="-1" role="dialog" data-backdrop="static">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="svg-container text-center">    
								<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
									<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
									<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
								</svg>
							</div>								
							<h3 class="m--font-success text-center">Uploaded Success!</h3>
							<div class="text-center"><a href="<?php echo base_url().'users/edittemplates1/'.$template->id;?>"  class="btn btn-default mt-2">OK</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="error_msg" tabindex="-1" role="dialog" data-backdrop="static">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="svg-container text-center"> 
								<img height="100" alt="" src="<?php echo base_url(); ?>media/assets/images/warning.png">
							</div>								
							<h3 class="m--font-danger text-center">Warning!</h3>
							<p class="text-center" id="msg_cont">File type should MP3 format!</p>
							<div class="text-center"><a href="#" style="background-color: #ff6161;" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">OK</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal modal-bg fade" id="video-render-modal" tabindex="-1" role="dialog" data-backdrop="static">
				<div class="innerpage-decor">
					<div class="innerpage-circle1">
						<img src="<?php echo base_url();?>media/assets/images/circle1.png" alt="">
					</div>
					<div class="innerpage-circle2">
						<img src="<?php echo base_url();?>media/assets/images/circle2.png" alt="">
					</div>
					<div class="innerpage-circle3">
						<img src="<?php echo base_url();?>media/assets/images/circle3.png" alt="">
					</div>
				</div>
				<div class="modal-dialog" role="document" style="max-width:550px">
					<div class="modal-content gray-bg">
						<div class="modal-body" style="min-height:330px">
							 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							 </button>
							<div class="row">
								<label class="col-md-12 modal-text-style">Add Project Name</label>
								<div class="col-md-12 mt-25">
									<div class="text-field-style">
										<input placeholder="Enter a project name" type="text" name="pname" onblur="insert_data();" id="pname" class="form-control advanced-textarea" value="test test">
									</div>
								</div>
								<div class="col-md-12 text-center mt-65">
									<button type="submit" class="btn btn-primary mr-10" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);">Submit</button>
									<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<input placeholder="Enter a project name" name="check_page_name" type='hidden' id="check_page_name" class="form-control" value='0'>		
		</div>
		</form>
    </div>
    <!-- /#wrapper -->	
	<!-- JavaScript -->	
    <!-- jQuery -->
    <script src="<?php echo base_url();?>media/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/jquery.mkhplayer.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>media/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	<!-- Data table JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url();?>media/dist/js/jquery.slimscroll.js"></script>
	
	<!-- simpleWeather JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
	<script src="<?php echo base_url();?>media/dist/js/simpleweather-data.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?php echo base_url();?>media/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Toast JavaScript 
	<script src="<?php echo base_url();?>media/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>-->
	
	<!-- EChartJS JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/echarts-liquidfill.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- Init JavaScript -->
	<script src="<?php echo base_url();?>media/dist/js/init.js"></script>
	<script src="<?php echo base_url();?>media/dist/js/dashboard5-data.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/slick.min.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/wavesurfer.min.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/app.js"></script>
	
	<input type="hidden" value="<?php echo base_url();?>" id="base_url">
<script src="<?php echo base_url();?>media/assets/js/spectrum.js"></script>
<script src="<?php echo base_url();?>media/assets/js/font_definitions.js"></script>
<script src="<?php echo base_url();?>media/assets/js/jquery-ui.min-drag.js"></script>

<script>
  var kitchensink = { };
  var canvas = new fabric.Canvas('canvas',{
	 preserveObjectStacking: true,
	 controlsAboveOverlay: true,
	 selectionCompatibility: true,
	 selection: true
  });
</script>

<script src="<?php echo base_url();?>media/assets/js/kitchensink/utils.js"></script>
<script src="<?php echo base_url();?>media/assets/js/kitchensink/app_config.js"></script>
<script src="<?php echo base_url();?>media/assets/js/kitchensink/controller.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('video').mkhPlayer();
		});
		function pause_video(id)
		{
			var vid = "vidmodal"+id;
			$('#'+vid).on('hidden.bs.modal', function () {
				$('#videomkh'+id).get(0).pause();
			});
		}
	</script>	
<script>
var width = 100,
    perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
    EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
    time = parseInt((EstimatedTime/1000)%60)*100;

     
function progressbar()
{
	$('#video-render-modal').modal('hide');
	$('#preview').removeClass("active in");
	$('#music').removeClass("active in");
	$('#edit').removeClass("active in");
	$('#preview_submit').modal('show');
	//$('#preview_submit').addClass("active in");
	$('.nav-item').removeClass("active");
	$('#previewsubmit_tab').addClass("active");
	
	//alert(time);
    time = 50000;
	// Loadbar Animation
	$(".loadbar").animate({
	  width: width + "%"
	}, time);

	// Loadbar Glow Animation
	$(".glow").animate({
	  width: width + "%"
	}, time);

	// Percentage Increment Animation
	var PercentageID = $("#precent"),
			start = 0,
			end = 100,
			durataion = time;
			animateValue(PercentageID, start, end, durataion); 
	function animateValue(id, start, end, duration) {
	  
		var range = end - start,
		  current = start,
		  increment = end > start? 1 : -1,
		  stepTime = Math.abs(Math.floor(duration / range)),
		  obj = $(id);
		
		var timer = setInterval(function() {
			current += increment;
			$(obj).text(current + "%");
		  //obj.innerHTML = current;
			if (current == end) {
				clearInterval(timer);
			}
		}, stepTime);
	}
	// Fading Out Loadbar on Finised
	setTimeout(function(){
	$('.preloader-wrap').fadeOut(300);
	}, time);
	   
}   
  

$('#default_file').change(function(){    
    //on change event  
    formdata = new FormData();
    if($(this).prop('files').length > 0)
    {
        file =$(this).prop('files')[0];
        formdata.append("music", file);
    }
	$.ajax({
		url: '<?php echo base_url(); ?>users/insertuploadfile', // point to server-side controller method
		type: "POST",
		data: formdata,
		processData: false,
		contentType: false,
		success: function (data) {
			if(data == 'succ')
			{
				$('#success_message').modal('show');
				return false;
			}
			else
			{
				$('#error_msg').modal('show');
				return false;
			}
		}
	});
});
$(document).ready( function() {
	var textArray = [
		'Downloading the upload loader',
		'Ordering the pixels to get me a cup of coffee',
		'Going through your video with a fine-tooth comb',
		'Ordering the pixels to get me a cup of coffee',
		'Going through your video with a fine-tooth comb',
		'Ordering the pixels to get me a cup of coffee',
		'Going through your video with a fine-tooth comb',
		'Ordering the pixels to get me a cup of coffee',
		'Going through your video with a fine-tooth comb',
		'Ordering the pixels to get me a cup of coffee',
		'Going through your video with a fine-tooth comb',
		'All generalizations are false, including this one.'  /**/
	];
	$('.is-current').randomText( textArray, 5000); // ( array, interval, ["reload text or html"] )
});
// custom jquery plugin loadText()
$.fn.randomText = function( textArray, interval, randomEle, prevText ) {
	var obj = $(this);
	if( $('#text-content').length == 0 ){ obj.append('<div id="text-content">'); }
	var textCont = $('#text-content');
	if( typeof randomEle != 'undefined' ){ if( $('#randomizer').length == 0 ){ obj.append('<a href="javascript:;" id="randomizer"><em>' + randomEle ); } }
	textCont.fadeOut( 'slow', function() {
		var chosenText = random_array( textArray );
		while( chosenText == prevText ) { chosenText = random_array( textArray ); }
		textCont.empty().html( chosenText );
		textCont.fadeIn( 'slow' );
		sendText = chosenText;
	});
	timeOut = setTimeout( function(){ obj.randomText( textArray, interval, randomEle, sendText ); }, interval );
	$("#randomizer").click( function(){
		if( !textCont.is(':animated') ) { clearTimeout( timeOut ); obj.randomText( textArray, interval, randomEle, sendText );} // animation check prevents "too much recursion" error in jQuery 
	});
}
//public function
function random_array( aArray ) {
	var rand = Math.floor( Math.random() * aArray.length + aArray.length );
	var randArray = aArray[ rand - aArray.length ];
	return randArray;
}
function add_music(str,type) {
  $('#replace_music').html('<div style="font-size: 18px; font-weight: 600;border-radius: 15px;text-align: center;color: #ff1744;padding: 10px 0px;">Current Music : '+type+' - '+str+'</div>');
 // $('#replace_music').html('<div style="background-color: #0c101b;border-radius: 15px;text-align: center;color: #ffffff;padding: 10px 0px 5px;"><div class="col-md-4" style="padding-top: 15px;font-size: 18px;">Current Music : '+type+' - '+str+'</div><div class="col-md-8"><div id="waveform"></div></div><div class="clearfix"></div></div>');
  $('#audio_section').attr('src', '<?php echo base_url(); ?>media/uploads/footage/'+str+'');
  $('#music_name_val').val(str);
}
function upload_music(str) {
	if(str == 0)
	{
		$('#uploaded_music_library').hide();
		$('#music_library').show();
		$('#libray_music').css("color", "rgb(64, 120, 225)");
		$('#upload_music').css("color", "rgb(84, 95, 126)");
	}
	if(str == 1)
	{
		$('#music_library').hide();
		$('#uploaded_music_library').show();
		$('#libray_music').css("color", "rgb(84, 95, 126)");
		$('#upload_music').css("color", "rgb(64, 120, 225)");
	}
}

function  previous_step(id) 
{
	var imgname = document.getElementById("img"+id).src;
	addbgimage(imgname,id)
	get_id(id);
	active_img(id);
	$("#next_tab_music").css("display", "block");
	$("#next_music").css("display", "none");
}
function tab(id) 
{
	var str1 = document.getElementById("textname1").value;
	var desc1 = document.getElementById("desc1").value;
	var addimage1 = document.getElementById("addimage1").value;
	var str2 = document.getElementById("textname2").value;
	var desc2 = document.getElementById("desc2").value;
	var addimage2 = document.getElementById("addimage2").value;
	if(str1 && str2 && addimage1 && str2 && desc2 && addimage2)
	{
		if(id=='music')
		{
			$('#edit').removeClass("active in");
			$('#preview').removeClass("active in");
			$('#music').addClass("active in");
			$('.nav-item').removeClass("active");
			$('#music_tab').addClass("active");
			 $("#remove-bg").removeClass('banner-style').addClass('banner-style1');
		}
		if(id=='edit')
		{
			$('#preview').removeClass("active in");
			$('#music').removeClass("active in");
			$('#edit').addClass("active in");
			$('.nav-item').removeClass("active");
			$('#edit_tab').addClass("active");
		}
		if(id=='preview')
		{
			$('#music').removeClass("active in");
			$('#edit').removeClass("active in");
			$('#preview').addClass("active in");
			$('.nav-item').removeClass("active");
			$('#preview_tab').addClass("active");
		}
		if(id=='preview_submit')
		{
			$('#preview').removeClass("active in");
			$('#music').removeClass("active in");
			$('#edit').removeClass("active in");
			$('#preview_submit').addClass("active in");
			$('.nav-item').removeClass("active");
			$('#previewsubmit_tab').addClass("active");
		}
	}
	else
	{
		var error = 0;
		if(document.getElementById("textname1").value == "")
		{
			$("#textname1").css("border", "1px solid red");
			$('#textname1').attr('placeholder', "Textname1 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#textname1").css("border", "none");
		}
		if(document.getElementById("desc1").value == "")
		{
			$("#desc1").css("border", "1px solid red");
			$('#desc1').attr('placeholder', "Textname1 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#desc1").css("border", "none");
		}
		if(document.getElementById("addimage1").value == "")
		{
			$("#addimage1").css("border", "1px solid red");
			error+=1
		}
		else
		{
			$("#addimage1").css("border", "none");
		}
		if(document.getElementById("textname2").value == "")
		{
			$("#textname2").css("border", "1px solid red");
			$('#textname2').attr('placeholder', "Textname2 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#textname2").css("border", "none");
		}
		if(document.getElementById("desc2").value == "")
		{
			$("#desc2").css("border", "1px solid red");
			$('#desc2').attr('placeholder', "Desc2 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#desc2").css("border", "none");
		}
		if(document.getElementById("addimage2").value == "")
		{
			$("#addimage2").css("border", "1px solid red");
			error+=1
		}
		else
		{
			$("#addimage2").css("border", "none");
		}
		if(error > 0)
		{
			return false();
		}
	}
}
function next_step(id) 
{
	if(id == '1' || id == '2')
	{
		var imgname = document.getElementById("img"+id).src;
	}
	var str1 = document.getElementById("textname1").value;
	var desc1 = document.getElementById("desc1").value;
	var addimage1 = document.getElementById("addimage1").value;
	var str2 = document.getElementById("textname2").value;
	var addimage2 = document.getElementById("addimage2").value;
	if(str1 && desc1 && addimage1 && str2)
	{
		if((str1 != "" && desc1 != "" && addimage1 != "" && str2 != "") && (addimage2 == ""))
		{
			if(id == 2)
			{
				$("#next_tab_music").css("display", "none");
				$("#next_music").css("display", "block");
				addbgimage(imgname,id);
				get_id(id);
				active_img(id);
			}
			else
			{
				$("#next_tab_music").css("display", "block");
				$("#next_music").css("display", "none");
			}
			
		}
		else
		{
			if(id == 2)
			{
				$("#next_tab_music").css("display", "none");
				$("#next_music").css("display", "block");
			}
		}
		var error = 0;
		if(document.getElementById("textname1").value == "")
		{
			$("#textname1").css("border", "1px solid red");
			$('#textname1').attr('placeholder', "Textname1 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#textname1").css("border", "none");
		}
		if(document.getElementById("desc1").value == "")
		{
			$("#desc1").css("border", "1px solid red");
			$('#desc1').attr('placeholder', "Textname1 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#desc1").css("border", "none");
		}
		if(document.getElementById("addimage1").value == "")
		{
			$("#addimage1").css("border", "1px solid red");
			error+=1
		}
		else
		{
			$("#addimage1").css("border", "none");
		}
		if(document.getElementById("textname2").value == "")
		{
			$("#textname2").css("border", "1px solid red");
			$('#textname2').attr('placeholder', "Textname2 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#textname2").css("border", "none");
		}
		if(document.getElementById("addimage2").value == "")
		{
			$("#addimage2").css("border", "1px solid red");
			error+=1
		}
		else
		{
			$("#addimage2").css("border", "none");
			if(id == 'music')
			{
				$('#edit').removeClass("active in");
				$('#preview').removeClass("active in");
				$('#music').addClass("active in");
				$('.nav-item').removeClass("active");
				$('#music_tab').addClass("active");
				$("#remove-bg").removeClass('banner-style').addClass('banner-style1');
			}
			else if(id=='edit')
			{
				$('#preview').removeClass("active in");
				$('#music').removeClass("active in");
				$('#edit').addClass("active in");
				$('.nav-item').removeClass("active");
				$('#edit_tab').addClass("active");
			}
			else if(id=='preview')
			{
				$('#music').removeClass("active in");
				$('#edit').removeClass("active in");
				$('#preview').addClass("active in");
				$('.nav-item').removeClass("active");
				$('#preview_tab').addClass("active");
			}
			else if(id=='preview_submit')
			{
				$('#preview').removeClass("active in");
				$('#music').removeClass("active in");
				$('#edit').removeClass("active in");
				$('#preview_submit').addClass("active in");
				$('.nav-item').removeClass("active");
				$('#previewsubmit_tab').addClass("active");
			}
			else
			{
				get_id(id);
				active_img(id);
				addbgimage(imgname,id);
			}
		}
			if(error > 0)
			{
				return false();
			}
	}
	else
	{
		if((str1 != "" && desc1 != "" && addimage1 != "" && str2 != "") && (addimage2 == ""))
		{
			addbgimage(imgname,id);
			get_id(id);
			active_img(id);
		}
		else
		{
			
		}
		var error = 0;
		if(document.getElementById("textname1").value == "")
		{
			$("#textname1").css("border", "1px solid red");
			$('#textname1').attr('placeholder', "Textname1 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#textname1").css("border", "none");
		}
		if(document.getElementById("desc1").value == "")
		{
			$("#desc1").css("border", "1px solid red");
			$('#desc1').attr('placeholder', "Textname1 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#desc1").css("border", "none");
		}
		if(document.getElementById("addimage1").value == "")
		{
			$("#addimage1").css("border", "1px solid red");
			error+=1
		}
		else
		{
			$("#addimage1").css("border", "none");
		}
		if(document.getElementById("textname2").value == "")
		{
			$("#textname2").css("border", "1px solid red");
			$('#textname2').attr('placeholder', "Textname2 name should not be blank!.");
			error+=1
		}
		else
		{
			$("#textname2").css("border", "none");
		}
		if(document.getElementById("addimage2").value == "")
		{
			$("#addimage2").css("border", "1px solid red");
			error+=1
		}
		else
		{
			$("#addimage2").css("border", "none");
			
		}
		if(error > 0)
		{
			return false();
		}
	}
}
function get_id(id) 
{
	$(".checklayer").css("display", "none");
	$('#layer_text'+id).css("display", "block");
}
function active_img(id)
{
	var photo_fullsize =  $('#img'+id).attr('src');
	//alert(photo_fullsize);
	$('.slider-for img').attr('src', photo_fullsize);
	$(".screenspreview_thumbnail").removeClass("active");
	$(".flex-list").removeClass("slick-current");
	$('#current'+id).addClass("slick-current");
	$('#active'+id).addClass("active");
}
function insert_data() 
{
	var page_name = $('#pname').val();
	if(page_name)
	{
		$("#check_page_name").val(1);
	}
	else
	{
		$("#check_page_name").val(0);
	}
}
function check_project() 
{
	var page_name = $('#pname').val();
	if(page_name)
	{
		formdata = new FormData();
		formdata.append("page_name", page_name);
		$.ajax({
			url: '<?php echo base_url(); ?>users/chekproject', // point to server-side controller method
			type: "POST",
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data) {
				if(data == 'succ')
				{
					$('#video-render-modal').modal('hide');
					$("#check_page_name").val(1);
				}
				else
				{
					$("#pname").val("");
					$("#check_page_name").val(0);
					$("#pname").css("border", "1px solid red");
					$('#pname').attr('placeholder', "Project name should not be duplicate!.");
					$('#video-render-modal').modal('show');
				}
			}
		});
		if($('#check_page_name').val() == '1')
		{
			progressbar();
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		$("#pname").css("border", "1px solid red");
		$('#pname').attr('placeholder', "Project name should not be blank!.");
		$('#video-render-modal').modal('show');
		return false;
	}
}
function strlength_checker(field,replace_id,maxlength,id) 
{
	var str = document.getElementById(field).value;
	$('#'+replace_id).html(''+str.length+' / '+maxlength+' characters');
	if(str)
	{
		$("#div_text"+id).css("display", "block");
	}
	else
	{
		$("#div_text"+id).css("display", "none");
	}
	$('#select_text'+id).html(str);
}
function add_description(field,replace_id,maxlength,id) 
{
	var strs = document.getElementById(field).value;
	$('#'+replace_id).html(''+strs.length+' / '+maxlength+' characters');
	if(strs)
	{
		$("#div_desc"+id).css("display", "block");
	}
	else
	{
		$("#div_desc"+id).css("display", "none");
	}
	$('#desc_text'+id).html(strs);
}
//start business sider
	 /* $('.slider-for').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  asNavFor: '.slider-nav',
	  fade: true,
	  autoplay:false
	}); */
	$('.slider-nav').slick({
	  slidesToShow: 7,
	  slidesToScroll: 1,
	  //asNavFor: '.slider-for',
	  dots: false,
	  arrows: false,
	  centerMode: false,
	  focusOnSelect: true
	});
	/* $('.slider-for').on('afterChange', function(event,slick,i){
	  $('.slider-nav .slick-slide').eq(i).addClass('slick-current');    				 
	}); */
	
$(document).ready(function() {
	$('.slider-nav .screenspreview_thumbnail').click(function(e){
	  e.preventDefault();

	  var photo_fullsize =  $(this).find('img').attr('src');
	  $('.slider-for img').attr('src', photo_fullsize);
	});
	
	 $(".slider-nav .screenspreview_thumbnail").click(function () {
		$(".slider-nav .screenspreview_thumbnail").removeClass("active");
		$(this).addClass("active");
	});
});
	
	$(document).ready(function() {
		var table = $('#example').DataTable( {
			fixedHeader: true
		} );
	} );
	function play(type) {
		var audio = document.getElementById('audio'+type);
		if (audio.paused) {
			audio.play();
			$('#play'+type).removeClass('glyphicon-play-circle')
			$('#play'+type).addClass('glyphicon-pause')
		}else{
			audio.pause();
			audio.currentTime = 0
			$('#play'+type).addClass('glyphicon-play-circle')
			$('#play'+type).removeClass('glyphicon-pause')
		}
		audio.ontimeupdate = function(){
			$('.progress-bar-primary'+type).css('width', audio.currentTime / audio.duration * 100 + '%')
		}
	}
$(document).ready(function () {
    $('#music_tab').on('click', function () {
       $("#remove-bg").removeClass('banner-style').addClass('banner-style1');
    });
    $('#edit_tab').on('click', function () {
       $("#remove-bg").removeClass('banner-style1').addClass('banner-style');
    });
    $('#render_tab').on('click', function () {
       $("#remove-bg").removeClass('banner-style1').addClass('banner-style2');
    });
});		

$(document).ready(function () {
$('#music_tab, #next_tab_music').on('click', function () {
$('.wavebar').each(function(){
  //Generate unic id
  var id = '_' + Math.random().toString(36).substr(2, 9);
  var path = $(this).attr('data-path');
  
  //Set id to container
  $(this).find(".wave-container").attr("id", id);

  //Initialize WaveSurfer
  var wavesurfer = WaveSurfer.create({
      container: '#' + id,
        waveColor: '#243049',
        progressColor: '#FF1744',
        height: 30,
		barWidth: 2,
		barHeight: 2
  });
  
  //Load audio file
  wavesurfer.load(path);
  
  //Add button event
  $(this).find("span").click(function(){
  	wavesurfer.playPause();
  });
  	            // If you want a responsive mode (so when the user resizes the window)
            // the spectrum will be still playable
            window.addEventListener("resize", function(){
                // Get the current progress according to the cursor position
                var currentProgress = wavesurfer.getCurrentTime() / wavesurfer.getDuration();

                // Reset graph
                wavesurfer.empty();
                wavesurfer.drawBuffer();
                // Set original position
                wavesurfer.seekTo(currentProgress);

                // Enable/Disable respectively buttons
            }, false);
});
});

    });
	$(document).ready(function(){
    function alignModal(){
        var modalDialog = $(this).find(".modal-dialog");
        
        // Applying the top margin on modal dialog to align it vertically center
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }
    // Align modal when it is displayed
    $(".modal").on("shown.bs.modal", alignModal);
    
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $(".modal:visible").each(alignModal);
    });   
});	

</script>


<script src="<?php echo base_url();?>media/assets/js/fabric/customiseControls.js"></script>
<script>
(function() {
	fabric.Object.prototype.customiseCornerIcons({
		settings: {
			borderColor: '#cddefa',
			cornerSize: 12,
			cornerShape: 'circle',
			cornerBackgroundColor: '#cddefa',
			cornerPadding: 10,
			transparentCorners: true
		},
	}, function() {
		canvas.renderAll();
	} );
})();
</script>
<script>
$('[data-toggle="tooltip"]').tooltip();
  (function() {
	function renderVieportBorders() {
	  var ctx = canvas.getContext();
	  ctx.save();
	  ctx.fillStyle = 'rgba(0,0,0,0.1)';
	  ctx.fillRect(
		canvas.viewportTransform[4],
		canvas.viewportTransform[5],
		canvas.getWidth() * canvas.getZoom(),
		canvas.getHeight() * canvas.getZoom());
	  ctx.setLineDash([5, 5]);
	  ctx.strokeRect(
		canvas.viewportTransform[4],
		canvas.viewportTransform[5],
		canvas.getWidth() * canvas.getZoom(),
		canvas.getHeight() * canvas.getZoom());
	  ctx.restore();
	}
	canvas.on('object:selected', function(opt) {
		var target = opt.target;
		if (target._cacheCanvas) {
		}
	})
	canvas.on('mouse:wheel', function(opt) {
	  var e = opt.e;
	  if (!e.ctrlKey) {
		return;
	  }
	  var newZoom = canvas.getZoom() + e.deltaY / 300;
	  canvas.zoomToPoint({ x: e.offsetX, y: e.offsetY }, newZoom);
	  renderVieportBorders();
	  e.preventDefault();
	  return false;
	});
	var viewportLeft = 0,
		viewportTop = 0,
		mouseLeft,
		mouseTop,
		_drawSelection = canvas._drawSelection,
		isDown = false;
	canvas.on('mouse:down', function(options) {
	  if (options.e.altKey) {
		isDown = true;

		viewportLeft = canvas.viewportTransform[4];
		viewportTop = canvas.viewportTransform[5];
							
		mouseLeft = options.e.x;
		mouseTop = options.e.y;
		_drawSelection = canvas._drawSelection;
		canvas._drawSelection = function(){ };
		renderVieportBorders();
	  }
	});
	canvas.on('mouse:move', function(options) {
	  if (options.e.altKey && isDown) {
		var currentMouseLeft = options.e.x;
		var currentMouseTop = options.e.y;

		var deltaLeft = currentMouseLeft - mouseLeft,
			deltaTop = currentMouseTop - mouseTop;

		canvas.viewportTransform[4] = viewportLeft + deltaLeft;
		canvas.viewportTransform[5] = viewportTop + deltaTop;
		canvas.renderAll();
		renderVieportBorders();
	  }
	});
	canvas.on('mouse:up', function() {
	  canvas._drawSelection = _drawSelection;
	  isDown = false;
	});
  })();
</script>
<script>
  (function(){
	var mainScriptEl = document.getElementById('main');
	if (!mainScriptEl) return;
	var preEl = document.createElement('pre');
	var codeEl = document.createElement('code');
	codeEl.innerHTML = mainScriptEl.innerHTML;
	codeEl.className = 'language-javascript';
	preEl.appendChild(codeEl);
	document.getElementById('bd-wrapper').appendChild(preEl);
  })();
</script>
<script src="<?php echo base_url();?>media/assets/js/fabric/jspdf.min.js"></script>
<script>
(function() {
	fabric.util.addListener(fabric.window, 'load', function() {
		var canvas = this.__canvas || this.canvas,
		canvases = this.__canvases || this.canvases;
		canvas && canvas.calcOffset && canvas.calcOffset();
		if (canvases && canvases.length) {
		  for (var i = 0, len = canvases.length; i < len; i++) {
			canvases[i].calcOffset();
		  }
		}
	});
})();
function load_json (id){
$('#removebgid').css('display','block');
var json = $('#template'+id).val();
canvas.loadFromJSON(json, function(){canvas.renderAll();});
}

$("#text-string").keyup(function(){	  		
	var activeObject = canvas.getActiveObject();
	  if (activeObject && activeObject.type === 'text') {
		  activeObject.text = this.value;
		  canvas.renderAll();
	  }
});
//Add background color
function addcolor(colorcode)
{
	var activeObjects = canvas.getActiveObjects();
	canvas.discardActiveObject()
	if (activeObjects.length) {
	  canvas.remove.apply(canvas, activeObjects);
	}
	else
	{
		removeSpot(canvas, 123);
	}
	 var rect = new fabric.Rect({
			id: 123,
            width: 1000,
            height: 600,
            fill: colorcode,
            draggable: true
            }); 
          rect.lockMovementX = true;
            rect.lockMovementY = true; 
            rect.lockUniScaling = true; 
            rect.lockRotation = true;  

            canvas.add(rect);
        	canvas.setActiveObject(rect);
      var image = canvas.getActiveObject(canvas.item(0));
      image.moveTo(-1);
      canvas.discardActiveObject();
      canvas.renderAll();
      canvas.sendToBack(rect);	
}
</script>
<script>

//Add background image
function addbgimage(imgname,img_id)
{
	canvas.clear();
	fabric.Image.fromURL(imgname, function(oImg) {
	oImg.set({
			id : 123,
			hasRotatingPoint: false,
			selectable: false,
			evented: false
		});
	oImg.scaleToWidth(canvas.width);
	oImg.scaleToHeight(canvas.height);
	canvas.centerObject(oImg);
	canvas.add(oImg);
	canvas.setActiveObject(oImg);
      var image = canvas.getActiveObject();
      image.moveTo(-1);
      canvas.discardActiveObject();
      canvas.renderAll();
      canvas.sendToBack(oImg);
	});
	//next_step(''+img_id);
	var title = document.getElementById('textname1').value;
	if(title)
	{
		canvas.add(new fabric.Text(title, {
		id : 1,
		left: 170,
		top: 22,
		angle: 349,
		fontFamily: 'helvetica',
		fill: '#fff',
		scaleX: 0.5,
		scaleY: 0.5,
		fontSize: 36,
		fontWeight: 800,
		hasRotatingPoint: false,
		selectable: false,
		evented: false
	  }));
	}
	var title1 = document.getElementById('textname2').value;
	if(title1)
	{
		canvas.add(new fabric.Text(title1, {
		id : 55,
		left: 320,
		top: 280,
		angle: 349,
		fontFamily: 'helvetica',
		fill: '#fff',
		scaleX: 0.5,
		scaleY: 0.5,
		fontSize: 68,
		fontWeight: 800,
		hasRotatingPoint: false,
		selectable: false,
		evented: false
	  }));
	} 
	var title2 = document.getElementById('textname3').value;
	if(title2)
	{
		canvas.add(new fabric.Text(title2, {
		id : 56,
		left: 345,
		top: 318,
		angle: 349,
		fontFamily: 'helvetica',
		fill: '#000',
		scaleX: 0.5,
		scaleY: 0.5,
		fontSize: 64,
		fontWeight: 800,
		hasRotatingPoint: false,
		selectable: false,
		evented: false
	  }));
	} 
	var description = document.getElementById('desc1').value;
	if(description)
	{
		canvas.add(new fabric.Text(description, {
		id : 2,
		left: 372,
		top: 370,
		fontFamily: 'helvetica',
		fill: '#000',
		scaleX: 0.5,
		scaleY: 0.5,
		originX: 'center',
		originY: 'top',
		fontSize: 25,
		fontWeight: 400,
		textAlign: 'center',
		hasRotatingPoint: false,
		selectable: false,
		evented: false
	  }));
	}
	var imgElement = document.getElementById("addimage"+img_id).files[0];
	if(imgElement)
	{
		var data = URL.createObjectURL(imgElement);
		fabric.Object.prototype.transparentCorners = false;
		fabric.Image.fromURL(data, function(img) {
		var patternSourceCanvas = new fabric.StaticCanvas();
		patternSourceCanvas.add(img);
		patternSourceCanvas.renderAll();
		var pattern = new fabric.Pattern({
		  source: function() {
			patternSourceCanvas.setDimensions({
			  width: img.getScaledWidth(),
			  height: img.getScaledHeight()
			});
			patternSourceCanvas.renderAll();
			return patternSourceCanvas.getElement();
		  },
		  repeat: 'no-repeat'
		});
		
		var shape = new fabric.Circle({
			id:3,
			radius: 109.5,
			left: 262,
			top: 46.5,
			fill: pattern,
			objectCaching: false,
			hasRotatingPoint: false,
			selectable: false,
			evented: false
		});
		var ShapeWidth = shape.width;
		var ShapeHeight = shape.height;
		img.scaleToWidth(ShapeWidth);
		img.scaleToHeight(ShapeHeight);
		
		canvas.add(shape);
		
	  });
	}
}
// Remove object by id
function removeSpot(canvas, id) {
    canvas.forEachObject(function(obj) {
        if (obj.id && obj.id === id) canvas.remove(obj);
    });
}
function clarbackground()
{
	canvas.backgroundImage = 0;
    canvas.backgroundColor = '#FFFFFF';
	canvas.renderAll();
	$('#removebgid').css('display','none');
}


//for group code start
const fabricDblClick = function(objects, handler) {
	return function() {
		if (objects.clicked) handler(objects);
		else {
			objects.clicked = true;
			setTimeout(function() {
				objects.clicked = false;
			}, 500);
		}
	};
};

// ungroup objects in group
let groupItems = [];
let ungroup = function(group) {
	groupItems = group._objects;
	group._restoreObjectsState();
	canvas.remove(group);
	for (var i = 0; i < groupItems.length; i++) {
		canvas.add(groupItems[i]);
	}
	canvas.renderAll();
}; 
</script>
<script>
$('.showSingle').click(function(){
	$('.targetDiv').slideToggle();
	$('.targetDiv').hide();
	$('#div'+$(this).attr('target')).toggle();
	$('.showSingle').not(this).removeClass('active');
	$(this).toggleClass('active');
});
function close_div(id) {
    if(id === 1) {
        $("#div"+id).slideToggle();
		$('.showSingle').not(this).removeClass('active');
    }else{
		$('#div'+id).slideToggle();
		$('.showSingle').not(this).removeClass('active');
	}
}


//Object clone
$('#btnClone').on('click', function(){ 
	canvas.getActiveObject().clone(function(cloned) {
		_clipboard = cloned;
	});
	_clipboard.clone(function(clonedObj) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 10,
			top: clonedObj.top + 10,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj) {
				canvas.add(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		_clipboard.top += 10;
		_clipboard.left += 10;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
});


SCALE_FACTOR = 1.20;

//zoomIn
$('#zoomIn').on('click', function(event) {
	zoomIn();
});
//zoomOut
$('#zoomOut').on('click', function(event) {
	zoomOut();
});
 
 // button Reset Zoom
$("#btnResetZoom").on('click', function(event){
    resetZoom();
});

var j=1;
var k=20;
var percent=100;
function zoomIn() {
	j=j+1;
	if(j > 6)
	{
		return false;
	}
	else
	{
		percent = percent + k;
		$('#zoomOut').prop('disabled', false);
		$('#zoom-percent').html(percent);
		canvas.setZoom(canvas.getZoom()*SCALE_FACTOR);
		canvas.setHeight(canvas.getHeight() * SCALE_FACTOR);
		canvas.setWidth(canvas.getWidth() * SCALE_FACTOR);
			
		if(j == 6)
		{
			$('#zoomIn').prop('disabled', true);
		}
		
		canvas.renderAll(); 
	}
  }
  
function zoomOut (){
	j=j-1;
	if(j < -3)
	{
		return false;
	}
	else
	{
		percent = percent - k;
		$('#zoomIn').prop('disabled', false);
		$('#zoom-percent').html(percent);
		canvas.setZoom(canvas.getZoom()/SCALE_FACTOR);
		canvas.setHeight(canvas.getHeight() / SCALE_FACTOR);
		canvas.setWidth(canvas.getWidth() / SCALE_FACTOR);
		
		if(j == -3)
		{
			$('#zoomOut').prop('disabled', true);
		}
		
		canvas.renderAll();
	}
}

// Reset Zoom
function resetZoom() {
	canvas.setHeight(canvas.getHeight() /canvas.getZoom() );
    canvas.setWidth(canvas.getWidth() / canvas.getZoom() );
    canvas.setZoom(1);
	canvas.renderAll();
	k=20;
	percent=100; 
	j=1;
	$('#zoom-percent').html(percent);
	$('#zoomIn').prop('disabled', false);
	$('#zoomOut').prop('disabled', false);
    $('.canvas-container').find('canvas').each(function(){
        $(this).css('left', 0);
        $(this).css('top', 0);
    });
}
//Show/hide button on selection object
canvas.on('object:selected', function(o) {
  if (o.target.isType('image')) {
    $('#color').hide();
	$('#imgCrop').show();
  }
});
//Show/hide button on selection object
canvas.on('selection:cleared', function() {
    $('#color').show();
	$('#imgCrop').hide();
});






//Delete object on delete button press
$('body').keydown(function(e) {   
    switch (e.keyCode) {
        //case 46: // delete
        //var obj = canvas.getActiveObject();
        //canvas.remove(obj);
        //canvas.renderAll();
        //return false;
		 
		case 46:  / delete /
		var obj = canvas.getActiveObject();
        if(canvas.getActiveObject()){
			canvas.remove(obj);
			canvas.renderAll();
			return false;
        }
        break;
		case 127:  / delete /
		var obj = canvas.getActiveObject();
        if(canvas.getActiveObject()){
			canvas.remove(obj);
			canvas.renderAll();
			return false;
        }
        break;
		case 125:  / delete /
		var obj = canvas.getActiveObject();
        if(canvas.getActiveObject()){
			canvas.remove(obj);
			canvas.renderAll();
			return false;
        }
        break;
		/* case 51:  / delete /
		var obj = canvas.getActiveObject();
        if(canvas.getActiveObject()){
			canvas.remove(obj);
			canvas.renderAll();
			return false;
        }
        break; 
		case 8:  / delete /
		var obj = canvas.getActiveObject();
        if(canvas.getActiveObject()){
			canvas.remove(obj);
			canvas.renderAll();
			return false;
        }
        break; */ 
    }
    return; //using "return" other attached events will execute
});


//Add Image From Computer
document.getElementById('imgLoader').onchange = function handleImage(e) {

var reader = new FileReader();
  reader.onload = function (event){
    var imgObj = new Image();
    imgObj.src = event.target.result;
    imgObj.onload = function () {
      var image = new fabric.Image(imgObj);
      image.set({
            angle: 0,
            padding: 5,
            cornersize:0,
      });
      canvas.centerObject(image);
      canvas.add(image);
      canvas.renderAll();
    }
  }
  reader.readAsDataURL(e.target.files[0]);
}
// Add background image file From Computer
document.getElementById('BgimgLoader').addEventListener("change", function(e) {
	var reader = new FileReader();
  reader.onload = function (event){
    var imgObj = new Image();
    imgObj.src = event.target.result;
    imgObj.onload = function () {
      var image = new fabric.Image(imgObj);
      image.set({
            id: 123,
            angle: 0,
            padding: 5,
            cornersize:0,
      });
      canvas.centerObject(image);
      canvas.add(image);
	  image.center();
	  canvas.setActiveObject(image);
      var image = canvas.getActiveObject();
      image.moveTo(-1);
      canvas.discardActiveObject();
      canvas.renderAll();
      canvas.sendToBack(image);
      canvas.renderAll();
    }
  }
  var activeObjects = canvas.getActiveObjects();
	canvas.discardActiveObject()
	if (activeObjects.length) {
	  canvas.remove.apply(canvas, activeObjects);
	}
	else
	{
		removeSpot(canvas, 123);
	}
  reader.readAsDataURL(e.target.files[0]);
	fabric.util.loadImage(data, function(img) {
      
		shape.set('fill', new fabric.Pattern({
        source: img,
      }));
      canvas.renderAll();
	  });
	  var shape = new fabric.Rect({
		id:123,
		width: 1280,
		height: 720,
		left: 10,
		top: 300,
		fill:'transparent',
	  });
	  var activeObjects = canvas.getActiveObjects();
		canvas.discardActiveObject()
		if (activeObjects.length) {
		  canvas.remove.apply(canvas, activeObjects);
		}
		else
		{
			removeSpot(canvas, 123);
		}
	  canvas.add(shape);
	  shape.center();
	  canvas.setActiveObject(shape);
      var image = canvas.getActiveObject();
      image.moveTo(-1);
      canvas.discardActiveObject();
      canvas.renderAll();
      canvas.sendToBack(shape);
	resetZoom(); 
});
// mirror y axis
function flipY(){
    var active = canvas.getActiveObject();
	active.toggle('flipY');
    canvas.renderAll();
}
// mirror x axis
function flipX(){
    var active = canvas.getActiveObject();
    active.toggle('flipX');
    canvas.renderAll();
}  
function text_json_load (id){
var json = $('#text_template'+id).val();
canvas.loadFromJSON(json, function(){canvas.renderAll();});
}
 

</script>
<script>
//Window resize
$(window).resize(function() {
    $('.scrollbar-color').height($(window).height() - 100);
    $('.scrollbar-img-texture').height($(window).height() - 320);
    $('.scrollbar-bgimg').height($(window).height() - 320);
    $('.scrollbar-addtext').height($(window).height() - 240);
    $('.scrollbar-img').height($(window).height() - 200);
    $('.scrollbar-shape').height($(window).height() - 240);
    $('.scrollbar-clipart').height($(window).height() - 240);
});
$(window).trigger('resize');
//Scroll Bar
jQuery(function($) {
	$('.scrollbar#bgscroll .os-viewport').on('scroll', function() {
		if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
			var imgsearch = $('#imgsearch').val();
			var type_img = '2';
			$.ajax({
				url: '<?php echo base_url(); ?>admin/users/serachimg', // point to server-side controller method
				type: "POST",
				cache: false,
				data: { imgsearch:imgsearch,img_type:type_img },
				async: false,
				success: function (data) {
					$('#img_filter').append(data);
				},
			});
		}
	})
	
	$('.scrollbar#texscroll .os-viewport').on('scroll', function() {
		if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
			var imgsearch = $('#img_tex').val();
			if(img_tex == '')
			{
				img_tex = 'textures';
			}
			var type_img = '3';
			$.ajax({
				url: '<?php echo base_url(); ?>admin/users/serachimg', // point to server-side controller method
				type: "POST",
				cache: false,
				data: { imgsearch:imgsearch,img_type:type_img },
				async: false,
				success: function (data) {
					$('#img_texture').append(data);
				},
			});
		}
	})
	$('.scrollbar#overlayscroll .os-viewport').on('scroll', function() {
		if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
			var imgsearch = $('#ovlayimg').val();
			if(img_tex == '')
			{
				img_tex = 'textures';
			}
			var type_img = '1';
			$.ajax({
				url: '<?php echo base_url(); ?>admin/users/serachimg', // point to server-side controller method
				type: "POST",
				cache: false,
				data: { imgsearch:imgsearch,img_type:type_img },
				async: false,
				success: function (data) {
					$('#overlay_img').append(data);
				},
			});
		}
	})
});
function addtitle (field,replace_id,maxlength,id)
{
	var str = document.getElementById(field).value;
	removeSpot(canvas, 1);
	canvas.add(new fabric.Text(str, {
	id : 1,
    left: 170,
	top: 22,
	angle: 349,
    fontFamily: 'helvetica',
    fill: '#fff',
    scaleX: 0.5,
	scaleY: 0.5,
	fontSize: 36,
	fontWeight: 800,
	hasRotatingPoint: false,
	selectable: false,
    evented: false
  }));
  $('#'+replace_id).html(''+str.length+' / '+maxlength+' characters');
}
function addtitle1 (field,replace_id,maxlength,id)
{
	var str = document.getElementById(field).value;
	removeSpot(canvas, 55);
	canvas.add(new fabric.Text(str, {
	id : 55,
    left: 320,
	top: 280,
	angle: 349,
    fontFamily: 'helvetica',
    fill: '#fff',
    scaleX: 0.5,
	scaleY: 0.5,
	fontSize: 68,
	fontWeight: 800,
	hasRotatingPoint: false,
	selectable: false,
    evented: false
  }));
  $('#'+replace_id).html(''+str.length+' / '+maxlength+' characters');
}
function addtitle2 (field,replace_id,maxlength,id)
{
	var str = document.getElementById(field).value;
	removeSpot(canvas, 56);
	canvas.add(new fabric.Text(str, {
	id : 56,
    left: 345,
	top: 318,
	angle: 349,
    fontFamily: 'helvetica',
    fill: '#000',
    scaleX: 0.5,
	scaleY: 0.5,
	fontSize: 64,
	fontWeight: 800,
	hasRotatingPoint: false,
	selectable: false,
    evented: false
  }));
  $('#'+replace_id).html(''+str.length+' / '+maxlength+' characters');
}
function Adddescription (field,replace_id,maxlength,id)
{
	var str = document.getElementById(field).value;
	removeSpot(canvas, 2);
	var data = new fabric.Text(str, {
	id : 2,
    left: 372,
	top: 370,
    fontFamily: 'helvetica',
    fill: '#000',
    scaleX: 0.5,
	scaleY: 0.5,
	originX: 'center',
	originY: 'top',
	fontSize: 25,
	fontWeight: 400,
	textAlign: 'center',
	hasRotatingPoint: false,
	selectable: false,
    evented: false
  });
  // left: 200,
	// top: 360,
  //data.fillText("textAlign=center", 150, 120); 
  canvas.add(data);
  $('#'+replace_id).html(''+str.length+' / '+maxlength+' characters');
}

 fabric.Image.fromURL('<?php echo base_url();?>media/uploads/videothumb/house.png', function(oImg) {
oImg.set({
		id : 123,
		hasRotatingPoint: false,
		selectable: false,
        evented: false
	});
	oImg.scaleToWidth(canvas.width);
	oImg.scaleToHeight(canvas.height);
	canvas.centerObject(oImg);
	canvas.add(oImg).renderAll();
	//canvas.setActiveObject(oImg);

});
document.getElementById('addimage1').addEventListener("change", function(e) {
removeSpot(canvas, 3);
var data = URL.createObjectURL(event.target.files[0]);
  fabric.Object.prototype.transparentCorners = false;

  fabric.Image.fromURL(data, function(img) {
    var patternSourceCanvas = new fabric.StaticCanvas();
    patternSourceCanvas.add(img);
    patternSourceCanvas.renderAll();
    var pattern = new fabric.Pattern({
      source: function() {
        patternSourceCanvas.setDimensions({
          width: img.getScaledWidth(),
          height: img.getScaledHeight()
        });
        patternSourceCanvas.renderAll();
        return patternSourceCanvas.getElement();
      },
      repeat: 'no-repeat'
    });
	
	var shape = new fabric.Circle({
		id:3,
		radius: 109.5,
		left: 262,
		top: 46.5,
		fill: pattern,
		objectCaching: false,
		hasRotatingPoint: false,
		selectable: false,
        evented: false
	});
	var ShapeWidth = shape.width;
	var ShapeHeight = shape.height;
	img.scaleToWidth(ShapeWidth);
	img.scaleToHeight(ShapeHeight);
	
	canvas.add(shape);
    
  });
});
document.getElementById('addimage2').addEventListener("change", function(e) {
removeSpot(canvas, 3);
var data = URL.createObjectURL(event.target.files[0]);
  fabric.Object.prototype.transparentCorners = false;

  fabric.Image.fromURL(data, function(img) {
    var patternSourceCanvas = new fabric.StaticCanvas();
    patternSourceCanvas.add(img);
    patternSourceCanvas.renderAll();
    var pattern = new fabric.Pattern({
      source: function() {
        patternSourceCanvas.setDimensions({
          width: img.getScaledWidth(),
          height: img.getScaledHeight()
        });
        patternSourceCanvas.renderAll();
        return patternSourceCanvas.getElement();
      },
      repeat: 'no-repeat'
    });
	
	var shape = new fabric.Circle({
		id:3,
		radius: 109.5,
		left: 262,
		top: 46.5,
		fill: pattern,
		objectCaching: false,
		hasRotatingPoint: false,
		selectable: false,
        evented: false
	});
	var ShapeWidth = shape.width;
	var ShapeHeight = shape.height;
	img.scaleToWidth(ShapeWidth);
	img.scaleToHeight(ShapeHeight);
	
	canvas.add(shape);
    
  });
});
</script>
</body>

</html>
