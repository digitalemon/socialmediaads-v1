<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');
		$this->load->helper('cookie');
		$this->load->model('user');
	}
	public function index()
	{
		$userinfo = $this->session->userdata('ads_user_Id');
		//print_r($userinfo);exit;
		$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
			
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/dashboard/dashboard', $data);
			$this->load->view('user/dashboard/footer');
		}
		else
		{
			$this->load->view('user/login/login-1');
		}
	}
	function signup()
	{
		$this->load->view('user/login/register');
	}
	public function is_signup()
	{
		$form_array = array(
			'fname' => $_POST['fullname'],
			'user_email' => $_POST['email'],
			'user_phone' => $_POST['mobile'],
			'user_pass' => MD5($_POST['password']),
			'is_active' => 1,
			'created_date' => date('Y-m-d')
			); 
		$this->user->form_insert('vc_user',$form_array);
		//echo $this->db->last_query();exit;
		redirect('login', 'location');
	}
	public function is_signin()
	{
		$result = $this->user->user_login_check($_POST['email'],MD5($_POST['password']));
		//print_r($result);exit;
		$userinfo = $this->session->userdata('ads_user_Id');
		if(!empty($result)){
			
			$this->session->set_userdata('ads_user_Id',$result[0]->id);
			$this->session->set_userdata('ads_uname',$result[0]->fname);
			$this->session->set_userdata('ads_uemail',$result[0]->user_email);
			$userinfo = $this->session->userdata('ads_user_Id');
			$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
			//$this->session->set_flashdata('success_msg', 'Login successfully');
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/dashboard/dashboard', $data);
			$this->load->view('user/dashboard/footer');
		}
		elseif(!empty($userinfo))
		{
			$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
			//$this->session->set_flashdata('success_msg', 'Login successfully');
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/dashboard/dashboard', $data);
			$this->load->view('user/dashboard/footer');
		}
		else
		{
			$this->session->set_flashdata('error_msg', 'username/password not correct');
			$this->load->view('user/login/login-1');
		}
	}
	function signout()
	{
		$this->session->sess_destroy();
		delete_cookie("psdfiles");
		redirect('login', 'location');
	}
	public function myprofile(){
		$userinfo = $this->session->userdata('ads_user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/profile', $data);
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function forgetpassword(){
		$this->load->view('user/login/forgetpassword');	
	}
	function resetpassword()
	{
		$this->load->view('user/login/resetpassword');
	}
	function updatepass()
	{
		$new_pass = $this->input->post('new_pass');
		$con_pass = $this->input->post('con_pass');
		$user_id = $this->uri->segment(3);;
		$form_array = array(
		'user_pass' => md5($new_pass)
		); 
		$this->user->form_update('vc_user',$form_array,$user_id);
		redirect('login', 'location');
	}
	public function forgottenpassword(){
		$result = $this->user->user_email_check($_POST['email']);
		//$result[0]->user_email, $result[0]->uname
		if(!empty($result)){
		$this->email->from('info@converthink.in', 'Converthink Team');
		$this->email->to('sisirbehura@gmail.com');
		$this->email->subject('Photo Proffessional User Password Reset');
		$body = '
		Hi Sisir,
		You recently requested to reset your password for your Photo Proffessional Account.
		Click the button below to reset it.
		
		<a href="'.base_url().'login/resetpassword/'.$result[0]->id.'" target="_blank">Reset Your Password</a>
		
		if you did not request a password reset, please ignore this email or reply to let us know.
		this password reset is only valid for the next 30 minutes.
		
		Thanks,
		Converthink Team';
		$this->email->message($body);
		//print_r($this->email);
		$this->email->send();
		redirect('login', 'location');
		//exit;
		}
	}
	public function changepassword(){
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo)){
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/changepassword');
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function updatepassword(){
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo)){
			$form_array = array(
			'user_pass' => MD5($_POST['n_pwd'])
			); 
			$this->user->form_update('vc_user',$form_array,$userinfo);
			$this->session->set_flashdata('success_msg', 'Password changed successfully');
			redirect('login/myprofile', 'location');
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	public function updateprofile(){
		$userinfo = $this->session->userdata('ads_user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
			//print_r($_POST);exit;
			$profile_photo = $_FILES['profile_photo']['name'];
		
			if(isset($_FILES['profile_photo']['name']))
			{
				$config['upload_path'] = './media/uploads/profile_photo/';
				$config['allowed_types'] = 'jpg';
				
				$this->load->library('upload', $config, 'profile_photo');
				$this->profile_photo->initialize($config);
				if (!$this->profile_photo->do_upload('profile_photo')) 
				{
					print_r($this->profile_photo->display_errors());
				}
				else
				{
					$filepath = $this->profile_photo->data();
					$_POST['profile_photo'] = $filepath['file_name'];
				}
			}
			$form_array = array(
			'fname' => $_POST['fname'],
			'profile_photo' => $_POST['profile_photo'],
			'user_email' => $_POST['user_email'],
			'user_phone' => $_POST['mobile'],
			'user_address' => $_POST['address'],
			'updated_date' => date('Y-m-d')
			); 
			$this->user->form_update('vc_user',$form_array,$userinfo);
			$this->session->set_flashdata('success_msg', 'Your profile update successfully');
			redirect('users/settings', 'location');
		}
		else
		{
			redirect('login', 'location');
		} 
	}
}
