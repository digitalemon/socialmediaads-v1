<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->model('user');
		$this->load->library('pagination');
		$this->load->helper('url'); 
        $this->load->helper('file');
	}
	// Retrive All Setting Parameter
	public function settingparameter($id)
	{
		$footer_data[setting] = $this->user->getRecordById('vc_setting',$id);
		return $footer_data[setting]->value;
	}
	public function index()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/dashboard/dashboard',$data);
			$this->load->view('user/dashboard/footer');
		}
		else
		{
			redirect('login', 'location');
		}
	}
	
	public function updatepassword()  {
		$data['value'] = $this->settingparameter(1);
		//print_r($_POST);exit;
		$pwd = $_POST['pwd'];
		$userinfo = $this->session->userdata('ads_user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		$form_array = array(
		'user_pass' => MD5($pwd)
		); 
		 $this->user->form_update('vc_user',$form_array,$userinfo);
	}
	function settings()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/settings', $data);
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function chekproject(){
		$data['value'] = $this->settingparameter(1);
		$page_name = $_POST['page_name'];
		$data['videoplayer'] =$this->user->getRecordByFildName('vc_videolayer','layertext',$page_name);
		if(count($data[videoplayer]) > 0)
		{
			echo "error";
		}
		else{
			echo "succ";
		}
	}
	public function updateprofile(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
			//print_r($_POST);exit;
			/* $profile_photo = $_FILES['profile_photo']['name'];
		
			if(isset($_FILES['profile_photo']['name']))
			{
				$config['upload_path'] = './media/uploads/profile_photo/';
				$config['allowed_types'] = 'jpg';
				
				$this->load->library('upload', $config, 'profile_photo');
				$this->profile_photo->initialize($config);
				if (!$this->profile_photo->do_upload('profile_photo')) 
				{
					print_r($this->profile_photo->display_errors());
				}
				else
				{
					$filepath = $this->profile_photo->data();
					$_POST['profile_photo'] = $filepath['file_name'];
				}
			} */
			$form_array = array(
			'fname' => $_POST['fname'],
			'user_email' => $_POST['user_email'],
			'user_phone' => $_POST['mobile'],
			'user_address' => $_POST['address'],
			'updated_date' => date('Y-m-d')
			); 
			$this->user->form_update('vc_user',$form_array,$userinfo);
			$this->session->set_flashdata('success_msg', 'Your profile update successfully');
			redirect('users/profile', 'location');
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//upload music files
	public function insertuploadfile(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo)){
			if ($_FILES["music"]["error"] == UPLOAD_ERR_OK)
			{
				$config['upload_path'] = './media/uploads/audio_files/';
				$config['allowed_types'] = 'mp3';
				$config['max_size'] = '500000';
				$this->load->library('upload', $config, 'music');
				$this->music->initialize($config);
				if (!$this->music->do_upload(music)) 
				{
					print_r($this->music->display_errors());
				}
				else
				{
					$filepath = $this->music->data();
					$musicfile = $filepath['file_name'];
					$data_v = array(
					'user_id'=>$userinfo,
					'music_name'=>$musicfile,
					'is_active'=>1,
					'is_delete'=>0,
					'created_date'=>date("Y-m-d H:i:s")
					);
					$this->user->form_insert('vc_audio_library',$data_v);
					echo "succ";
				}
			}
			else{
				echo "err";
			}
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//for template page work start
	public function templates(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		//print_r($userinfo);exit;
		if(isset($userinfo)){
		$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
		$data['lavel_access'] =$this->user->Get_All_Niche_id($data['creat_script']->level_id);
		$category_array ='';
		foreach($data['lavel_access'] as $access_cat)
		{
			if($category_array)
			{
				$category_array .=",".$access_cat->niche_id;
			}
			else{
				$category_array .=$access_cat->niche_id;
			}
		}
		//print_r($category_array);exit;
		if(empty($_POST['search_val']))
		{
			$search_val = '';
			$data['templates'] = $this->user->Get_All_Access_Category($category_array,$search_val);
			//print_r($data); exit;
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/templates', $data);	
			$this->load->view('user/dashboard/footer');
		}
		else
		{
			if($_POST['search_val'] != "blank_value")
			{
			$array_where = array('tname' => $_POST['search_val']);
			}
			$data['templates'] = $this->user->Get_All_Access_Category($category_array,$array_where);
			$this->load->view('user/search-templates', $data);		
		}
		}else{
			redirect('login', 'location');
		} 
	}
	public function edittemplates(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		//print_r($userinfo);exit;
		$id=$this->uri->segment(3);
		if(isset($userinfo)){
		
		$data['templatescene'] = array('');
		$data['videoplayer'] = array('');
		array_push($data[templatescene],$this->user->where_fetch_data('vc_templatescene', '', '', 'temp_id', $id));
		$data['templatesce'] = $this->user->where_fetch_data('vc_templatescene', '', '', 'temp_id', $id);
		foreach($data[templatesce] as $templatesce)
		{
			array_push($data[videoplayer],$this->user->getRecordByFildName('vc_videolayer','tempid',$templatesce->id));
		}
		$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
		$data['uploadedaudio'] =$this->user->getRecordByFildName('vc_audio_library','user_id',$userinfo);
		$this->load->view('user/addtemplate', $data);
		}else{
			redirect('login', 'location');
		} 
	}
	public function edittemplates1(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		//print_r($userinfo);exit;
		//echo $_COOKIE['scene_item'];
		$explode_scene = explode(",",$_COOKIE['scene_item']);
		$id=$this->uri->segment(3);
		if(isset($userinfo))
		{
			$data['templatescene'] = array('');
			$data['videoplayer'] = array('');
			for($i=0; $i <count($explode_scene); $i++)
			{
				array_push($data[templatescene],$this->user->getRecordByFildName('vc_templatescene','id',$explode_scene[$i]));
				array_push($data[videoplayer],$this->user->getRecordByFildName('vc_videolayer','tempid',$explode_scene[$i]));
			}
			$data['audioplayer'] = $this->user->getRecordByFildName('vc_audio_library','user_id','0');
			$data['uploadedaudio'] = $this->user->getRecordByFildName('vc_audio_library','user_id',$userinfo);
			//print_r($data);exit;
			$this->load->view('user/addtemplate1', $data);
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//for template page work end
	public function projects(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			ini_set('max_execution_time', 300);
			if(!empty($_POST['tid']))
			{
				$tid=$_POST['tid'];
				$template = $this->user->getRecordById('vc_videotemplate', $tid);
				$string = file_get_contents(base_url().'media/uploads/jsonfile/ctsagain.json');
				$json_string = json_decode($string);
				$txt = $_POST['textname1'];
				$desc1 = $_POST['desc1'];
				$projname = $_POST['pname'];
				$json_string[0]->Text1 = $txt;
				$json_string[0]->Text2 = $desc1;
				// Copy bg Image file to coresponding scene image
				$addimg2 = $_POST['addimg2'];
				$addimg1 = $_POST['addimg1'];
				if(!empty($addimg1))
				{
					if(file_exists('media/uploads/footage/fashion_'.$userinfo.'_scene_1.png'))
					{
						$prev_file_path = "./media/uploads/footage/".$userinfo."_scene_1.png";
						unlink($prev_file_path);
					}
					$file_name = "fashion_".$userinfo."_scene_1.png";
					$dst = APPPATH . "../media/uploads/footage/";
					$file = fopen($dst."/".$file_name, 'w') or die('Error opening file: '+$file_name);
					$data = explode( ',', $addimg1 );
					fwrite($file,base64_decode( $data[ 1 ] ));
					fclose($file);
					$json_string[0]->Image1placeholder = base_url().'media/uploads/footage/'.$file_name;
				}
				if(!empty($addimg2))
				{
					if(file_exists('media/uploads/footage/fashion_'.$userinfo.'_scene_2.png'))
					{
						$prev_file_path = "./media/uploads/footage/fashion_".$userinfo."_scene_2.png";
						unlink($prev_file_path);
					}
					$file_name = "fashion_".$userinfo."_scene_2.png";
					$dst = APPPATH . "../media/uploads/footage/";
					$file = fopen($dst."/".$file_name, 'w') or die('Error opening file: '+$file_name);
					$data = explode( ',', $addimg2 );
					fwrite($file,base64_decode( $data[ 1 ] ));
					fclose($file);
					$json_string[0]->Image2placeholder = base_url().'media/uploads/footage/'.$file_name;
				}
				$json_string[0]->ID = $projname;
				//echo $_POST['music_name_val'];exit;
				if(!empty($_POST['music_name_val']))
				{
					$musicName = $_POST['music_name_val'];
					$musicdst = APPPATH . "../media/uploads/audio_files/";
				}
				//print_r($json_string);exit;
				$json_sting_encode = json_encode($json_string);
				$dst = APPPATH . "../media/uploads/jsonfile";
				$dstaep = APPPATH . "../media/uploads/video_file";
				//exit();
				$fileLocation = $projname.".json";
				$file = fopen($dst."/".$fileLocation,"w+");
				fwrite($file,$json_sting_encode);
				//file_put_contents($file, $json_sting_encode);
				fclose($file); 
				//exit;
				$tstring = file_get_contents(base_url().'media/uploads/jsonfile/templater-options.json');
				$tjson_string = json_decode($tstring);
				//$template_name = $template->tname;
				$template_name = "Fashion";
				//$template_aep = $template->tfname;
				$template_aep = "Fashion.aep";
				
				$tjson_string->prefs->default_target = $template_name;
				$tjson_string->data_source = 'C:\\Templater\\input\\'.$fileLocation;
				$tjson_string->aep = 'C:\\Templater\\input\\'.$template_aep;
				$tjson_sting_encode = json_encode($tjson_string);
				$tfile = $dst.'/templater-options.json';
				file_put_contents($tfile, $tjson_sting_encode);
				$generatefname = $tjson_string->output_prefix.'_'.$tjson_string->prefs->default_target.'_'.$projname.'.avi';
				$ffmpegpath = "C:\\Templater\\ffmpeg\\bin\\ffmpeg.exe";
				$outfilename = $tjson_string->output_prefix.'_'.$projname.'.mp4';
				if($musicName != '')
				{
					$generateoutputwithaudio = $projname.'.mp4';
				}
				else
				{
					$generateoutputwithaudio = $outfilename;
				}
	
				$sting .= 'Copy-Item -Path '.$dst.'/'.$fileLocation.' -Destination C:\\Templater\\input\\'.$fileLocation. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dstaep.'/'.$template_aep.' -Destination C:\\Templater\\input\\'.$template_aep. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dst.'/templater-options.json -Destination C:\Templater\input\templater-options.json'. PHP_EOL;
				$sting .= 'C:\Templater\input\templater.ps1 -v "CC 2019"'. PHP_EOL;
				$sting .= $ffmpegpath.' -y -i C:\\Templater\\output\\'.$generatefname.' -c:v libx264 -c:a aac -pix_fmt yuv420p -movflags faststart -hide_banner C:\\Templater\\output\\'.$outfilename. PHP_EOL;
				//$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$outfilename.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$outfilename. PHP_EOL;
				if($musicName != '')
				{
				$sting .= 'Copy-Item -Path '.$musicdst.$musicName.' -Destination C:\\Templater\\input\\'.$musicName. PHP_EOL;
				$sting .= $ffmpegpath.' -i C:\\Templater\\output\\'.$outfilename.' -i C:\\Templater\input\\'.$musicName.' -map 0:v -map 1:a -c copy -shortest C:\\Templater\\output\\'.$generateoutputwithaudio. PHP_EOL;
				}
				$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$generateoutputwithaudio.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio. PHP_EOL;
				
				//echo $sting;exit;
				$dstp = APPPATH . "../media/uploads/powershellfile";
				$pfile = $dstp.'/temp.ps1';
				file_put_contents($pfile, $sting);
				Shell_Exec ('start /WAIT C:\Windows\WinSxS\wow64_microsoft-windows-powershell-exe_31bf3856ad364e35_10.0.14393.206_none_ad6ee618d45c7fca\powershell.exe  -command C:\xampp\htdocs\socialmediaads-v1\media\uploads\powershellfile\temp.ps1 2>&1 &');
				
				if (file_exists('C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio))
				{
					$data_v = array(
					'userid'=>$userinfo,
					'tempid'=>$tid,
					'uvidname'=>$generateoutputwithaudio,
					'is_active'=>1,
					'is_delete'=>0,
					'created_date'=>date("Y-m-d H:i:s"),
					'updated_date'=>''
					);
					$this->user->form_insert('vc_project',$data_v);
				}
				if(file_exists('media/uploads/footage/fashion_'.$userinfo.'_scene_1.png'))
				{
					$prev_file_path = "./media/uploads/footage/fashion_".$userinfo."_scene_1.png";
					unlink($prev_file_path);
				}
				if(file_exists('media/uploads/footage/fashion_'.$userinfo.'_scene_2.png'))
				{
					$prev_file_path = "./media/uploads/footage/fashion_".$userinfo."_scene_2.png";
					unlink($prev_file_path);
				}
			}
			$data['projects'] = $this->user->where_fetch_data('vc_project', '', '', 'userid', $userinfo);
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/projects', $data);	
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function projectdelete(){
		$id=$_POST['project_id'];
		$projects = $this->user->delete('vc_project',$id);
		echo "succ";
	}
	
	public function trainings(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		//print_r($userinfo);exit;
		if(isset($userinfo)){
		$data['trained'] = $this->user->get_all_data('vc_training');
		//print_r($data['projects']); exit;
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/training', $data);	
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function support(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo)){
		$data['trained'] = $this->user->get_all_data('vc_support');
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/support', $data);	
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	function changepassword()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/changepwd', $data);
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	function changepwd()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$oldpwd = $_POST['oldpwd'];
			$newpwd = $_POST['newpwd'];
			$cpwd = $_POST['cpwd'];
			$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
			$form_array = array(
			'user_pass' => MD5($cpwd)
			); 
			 $this->user->form_update('vc_user',$form_array,$userinfo);
			 //redirect('users/changepassword', 'location');
			 echo "succ";
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	public function checkoldpdw(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$oldpwd = MD5($_POST['oldpwd']);
			if(isset($_POST['oldpwd']) && $_POST['oldpwd'] != ""){
				$result = $this->user->getRecordByFildNameId('vc_user', 'user_pass', $oldpwd, $userinfo);
				//echo count($result); exit;
				if(count($result)> 0)
				{
					echo $msg = "succ";
				}
				else
				{
					echo $msg = "error";
				}
			}
			else
			{
				echo $msg = "blank";
			}
		}
		else
		{
			redirect('admin', 'location');
		} 
	} 
	public function newtemplate(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$this->load->view('user/addnewtemplate');
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	public function fashiontemplate(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
			$this->load->view('user/fashiontemplate',$data);
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	public function fjtemplate(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
			$this->load->view('user/fjtemplate',$data);
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//for template page work end
	public function projectsdental (){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			ini_set('max_execution_time', 300);
			if(!empty($_POST['tid']))
			{
				$tid=$_POST['tid'];
				$template = $this->user->getRecordById('vc_videotemplate', $tid);
				//$templatelayer = $this->user->getRecordByFildName('vc_videolayer', 'tempid', $template->id);
				//$layername = $templatelayer[0]->layername;
				//$string = file_get_contents(base_url().'media/uploads/jsonfile/'.$template->jsonfile);
				$string = file_get_contents(base_url().'media/uploads/jsonfile/dental.json');
				$json_string = json_decode($string);
				$txt1 = $_POST['textname1'];
				$txt2 = $_POST['textname2'];
				$desc1 = $_POST['desc1'];
				$projname = $_POST['pname'];
				
				$json_string[0]->Text1 = $txt1;
				$json_string[0]->Text2 = $txt2;
				$json_string[0]->Text3 = $desc1;
				// Copy bg Image file to coresponding scene image
				if(!empty($_FILES['addimage1']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addimage1');
					$this->addimage1->initialize($config);
					if (!$this->addimage1->do_upload('addimage1')) 
					{
						print_r($this->addimage1->display_errors());
					}
					else
					{
						$filepath = $this->addimage1->data();
						$addimage1 = $filepath['file_name'];
					}
					$json_string[0]->placeholderimage6 = base_url().'media/uploads/footage/'.$addimage1;
				}
				if(!empty($_FILES['addtwo1']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addtwo1');
					$this->addtwo1->initialize($config);
					if (!$this->addtwo1->do_upload('addtwo1')) 
					{
						print_r($this->addtwo1->display_errors());
					}
					else
					{
						$filepath = $this->addtwo1->data();
						$addtwo1 = $filepath['file_name'];
					}
					$json_string[0]->placeholderimage5 = base_url().'media/uploads/footage/'.$addtwo1;
				}
				if(!empty($_FILES['addthree1']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addthree1');
					$this->addthree1->initialize($config);
					if (!$this->addthree1->do_upload('addthree1')) 
					{
						print_r($this->addthree1->display_errors());
					}
					else
					{
						$filepath = $this->addthree1->data();
						$addthree1 = $filepath['file_name'];
					}
					$json_string[0]->placeholderimage4 = base_url().'media/uploads/footage/'.$addthree1;
				}
				if(!empty($_FILES['addimage2']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addimage2');
					$this->addimage2->initialize($config);
					if (!$this->addimage2->do_upload('addimage2')) 
					{
						print_r($this->addimage2->display_errors());
					}
					else
					{
						$filepath = $this->addimage2->data();
						$addimage2 = $filepath['file_name'];
					}
					$json_string[0]->placeholderimage3 = base_url().'media/uploads/footage/'.$addimage2;
				}
				if(!empty($_FILES['addtwo2']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addtwo2');
					$this->addtwo2->initialize($config);
					if (!$this->addtwo2->do_upload('addtwo2')) 
					{
						print_r($this->addtwo2->display_errors());
					}
					else
					{
						$filepath = $this->addtwo2->data();
						$addtwo2 = $filepath['file_name'];
					}
					$json_string[0]->placeholderimage2 = base_url().'media/uploads/footage/'.$addtwo2;
				}
				if(!empty($_FILES['addthree2']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addthree2');
					$this->addthree2->initialize($config);
					if (!$this->addthree2->do_upload('addthree2')) 
					{
						print_r($this->addthree2->display_errors());
					}
					else
					{
						$filepath = $this->addthree2->data();
						$addthree2 = $filepath['file_name'];
					}
					$json_string[0]->placeholderimage1 = base_url().'media/uploads/footage/'.$addthree2;
				}
				
				$json_string[0]->ID = $projname;
				//echo $_POST['music_name_val'];exit;
				if(!empty($_POST['music_name_val']))
				{
					$musicName = $_POST['music_name_val'];
					$musicdst = APPPATH . "../media/uploads/audio_files/";
				}
				//print_r($json_string);exit;
				$json_sting_encode = json_encode($json_string);
				$dst = APPPATH . "../media/uploads/jsonfile";
				$dstaep = APPPATH . "../media/uploads/video_file";
				//exit();
				$fileLocation = $projname.".json";
				$file = fopen($dst."/".$fileLocation,"w+");
				fwrite($file,$json_sting_encode);
				//file_put_contents($file, $json_sting_encode);
				fclose($file); 
				//exit;
				$tstring = file_get_contents(base_url().'media/uploads/jsonfile/templater-options.json');
				$tjson_string = json_decode($tstring);
				//print_r($tjson_string);exit;
				//$template_name = $template->tname;
				$template_name = "Dental";
				//$template_aep = $template->tfname;
				$template_aep = "Dental.aep";
				
				$tjson_string->prefs->default_target = $template_name;
				$tjson_string->data_source = 'C:\\Templater\\input\\'.$fileLocation;
				$tjson_string->aep = 'C:\\Templater\\input\\'.$template_aep;
				$tjson_sting_encode = json_encode($tjson_string);
				$tfile = $dst.'/templater-options.json';
				file_put_contents($tfile, $tjson_sting_encode);
				$generatefname = $tjson_string->output_prefix.'_'.$tjson_string->prefs->default_target.'_'.$projname.'.avi';
				//print_r($tjson_string);exit;
				$ffmpegpath = "C:\\Templater\\ffmpeg\\bin\\ffmpeg.exe";
				$outfilename = $tjson_string->output_prefix.'_'.$projname.'.mp4';
				if($musicName != '')
				{
					$generateoutputwithaudio = $projname.'.mp4';
				}
				else
				{
					$generateoutputwithaudio = $outfilename;
				}
	
				$sting .= 'Copy-Item -Path '.$dst.'/'.$fileLocation.' -Destination C:\\Templater\\input\\'.$fileLocation. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dstaep.'/'.$template_aep.' -Destination C:\\Templater\\input\\'.$template_aep. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dst.'/templater-options.json -Destination C:\Templater\input\templater-options.json'. PHP_EOL;
				$sting .= 'C:\Templater\input\templater.ps1 -v "CC 2019"'. PHP_EOL;
				$sting .= $ffmpegpath.' -y -i C:\\Templater\\output\\'.$generatefname.' -c:v libx264 -c:a aac -pix_fmt yuv420p -movflags faststart -hide_banner C:\\Templater\\output\\'.$outfilename. PHP_EOL;
				//$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$outfilename.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$outfilename. PHP_EOL;
				if($musicName != '')
				{
				$sting .= 'Copy-Item -Path '.$musicdst.$musicName.' -Destination C:\\Templater\\input\\'.$musicName. PHP_EOL;
				$sting .= $ffmpegpath.' -i C:\\Templater\\output\\'.$outfilename.' -i C:\\Templater\input\\'.$musicName.' -map 0:v -map 1:a -c copy -shortest C:\\Templater\\output\\'.$generateoutputwithaudio. PHP_EOL;
				}
				$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$generateoutputwithaudio.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio. PHP_EOL;
				
				//echo $sting;exit;
				$dstp = APPPATH . "../media/uploads/powershellfile";
				$pfile = $dstp.'/temp.ps1';
				file_put_contents($pfile, $sting);
				Shell_Exec ('start /WAIT C:\Windows\WinSxS\wow64_microsoft-windows-powershell-exe_31bf3856ad364e35_10.0.14393.206_none_ad6ee618d45c7fca\powershell.exe  -command C:\xampp\htdocs\socialmediaads-v1\media\uploads\powershellfile\temp.ps1 2>&1 &');
				
				if (file_exists('C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio))
				{
					$data_v = array(
					'userid'=>$userinfo,
					'tempid'=>$tid,
					'uvidname'=>$generateoutputwithaudio,
					'is_active'=>1,
					'is_delete'=>0,
					'created_date'=>date("Y-m-d H:i:s"),
					'updated_date'=>''
					);
					$this->user->form_insert('vc_project',$data_v);
				}
			}
			$data['projects'] = $this->user->where_fetch_data('vc_project', '', '', 'userid', $userinfo);
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/projects', $data);	
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function traveltemplate(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
			$this->load->view('user/traveltemplate',$data);
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//for Travel template page work end
	public function travelprojects(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo)){
			ini_set('max_execution_time', 300);
			$_POST['tid'] = '1';
			//echo $_POST['tid'];exit;
			if(!empty($_POST['tid']))
			{
				$tid=$_POST['tid'];
				$template = $this->user->getRecordById('vc_videotemplate', $tid);
				//$templatelayer = $this->user->getRecordByFildName('vc_videolayer', 'tempid', $template->id);
				//$layername = $templatelayer[0]->layername;
				//$string = file_get_contents(base_url().'media/uploads/jsonfile/'.$template->jsonfile);
				$string = file_get_contents(base_url().'media/uploads/jsonfile/traveljson.json');
				$json_string = json_decode($string);
				$txt = $_POST['textname1'];
				$desc1 = $_POST['desc1'];
				$projname = $_POST['pname'];
				$json_string[0]->Text2 = $txt;
				$json_string[0]->Text1 = $desc1;
				// Copy bg Image file to coresponding scene image
				if(!empty($_FILES['addimage1']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = '*';
					$config['max_size'] = '50000';
					$this->load->library('upload', $config, 'addimage1');
					$this->addimage1->initialize($config);
					if (!$this->addimage1->do_upload('addimage1')) 
					{
						print_r($this->addimage1->display_errors());
					}
					else
					{
						$filepath = $this->addimage1->data();
						$addimage1 = $filepath['file_name'];
					}
				}
				if(!empty($addimage1))
				{
					$json_string[0]->video1 = base_url().'media/uploads/footage/'.$addimage1;
				}
				$json_string[0]->ID = $projname;
				//echo $_POST['music_name_val'];exit;
				if(!empty($_POST['music_name_val']))
				{
					$musicName = $_POST['music_name_val'];
					$musicdst = APPPATH . "../media/uploads/audio_files/";
				}
				//print_r($json_string);exit;
				$json_sting_encode = json_encode($json_string);
				$dst = APPPATH . "../media/uploads/jsonfile";
				$dstaep = APPPATH . "../media/uploads/video_file";
				//exit();
				$fileLocation = $projname.".json";
				$file = fopen($dst."/".$fileLocation,"w+");
				fwrite($file,$json_sting_encode);
				//file_put_contents($file, $json_sting_encode);
				fclose($file); 
				//exit;
				$tstring = file_get_contents(base_url().'media/uploads/jsonfile/templater-options.json');
				$tjson_string = json_decode($tstring);
				//print_r($tjson_string);exit;
				//$template_name = $template->tname;
				$template_name = "Fashion";
				//$template_aep = $template->tfname;
				$template_aep = "Fashion.aep";
				
				$tjson_string->prefs->default_target = $template_name;
				$tjson_string->data_source = 'C:\\Templater\\input\\'.$fileLocation;
				$tjson_string->aep = 'C:\\Templater\\input\\'.$template_aep;
				$tjson_sting_encode = json_encode($tjson_string);
				$tfile = $dst.'/templater-options.json';
				file_put_contents($tfile, $tjson_sting_encode);
				$generatefname = $tjson_string->output_prefix.'_'.$tjson_string->prefs->default_target.'_'.$projname.'.avi';
				//print_r($tjson_string);exit;
				$ffmpegpath = "C:\\Templater\\ffmpeg\\bin\\ffmpeg.exe";
				$outfilename = $tjson_string->output_prefix.'_'.$projname.'.mp4';
				if($musicName != '')
				{
					$generateoutputwithaudio = $projname.'.mp4';
				}
				else
				{
					$generateoutputwithaudio = $outfilename;
				}
	
				$sting .= 'Copy-Item -Path '.$dst.'/'.$fileLocation.' -Destination C:\\Templater\\input\\'.$fileLocation. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dstaep.'/'.$template_aep.' -Destination C:\\Templater\\input\\'.$template_aep. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dst.'/templater-options.json -Destination C:\Templater\input\templater-options.json'. PHP_EOL;
				$sting .= 'C:\Templater\input\templater.ps1 -v "CC 2019"'. PHP_EOL;
				$sting .= $ffmpegpath.' -y -i C:\\Templater\\output\\'.$generatefname.' -c:v libx264 -c:a aac -pix_fmt yuv420p -movflags faststart -hide_banner C:\\Templater\\output\\'.$outfilename. PHP_EOL;
				//$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$outfilename.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$outfilename. PHP_EOL;
				if($musicName != '')
				{
				$sting .= 'Copy-Item -Path '.$musicdst.$musicName.' -Destination C:\\Templater\\input\\'.$musicName. PHP_EOL;
				$sting .= $ffmpegpath.' -i C:\\Templater\\output\\'.$outfilename.' -i C:\\Templater\input\\'.$musicName.' -map 0:v -map 1:a -c copy -shortest C:\\Templater\\output\\'.$generateoutputwithaudio. PHP_EOL;
				}
				$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$generateoutputwithaudio.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio. PHP_EOL;
				
				//echo $sting;exit;
				$dstp = APPPATH . "../media/uploads/powershellfile";
				$pfile = $dstp.'/temp.ps1';
				file_put_contents($pfile, $sting);
				Shell_Exec ('start /WAIT C:\Windows\WinSxS\wow64_microsoft-windows-powershell-exe_31bf3856ad364e35_10.0.14393.206_none_ad6ee618d45c7fca\powershell.exe  -command C:\xampp\htdocs\socialmediaads-v1\media\uploads\powershellfile\temp.ps1 2>&1 &');
				
				if (file_exists('C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio))
				{
					$data_v = array(
					'userid'=>$userinfo,
					'tempid'=>$tid,
					'uvidname'=>$generateoutputwithaudio,
					'is_active'=>1,
					'is_delete'=>0,
					'created_date'=>date("Y-m-d H:i:s"),
					'updated_date'=>''
					);
					$this->user->form_insert('vc_project',$data_v);
				}
			}
			$data['projects'] = $this->user->where_fetch_data('vc_project', '', '', 'userid', $userinfo);
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/projects', $data);	
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function hptemplate(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
			$this->load->view('user/housepainttemplate',$data);
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//for template page work end
	public function projectshp (){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			ini_set('max_execution_time', 300);
			$_POST['tid'] = '1';
			if(!empty($_POST['tid']))
			{
				$tid=$_POST['tid'];
				$template = $this->user->getRecordById('vc_videotemplate', $tid);
				//$templatelayer = $this->user->getRecordByFildName('vc_videolayer', 'tempid', $template->id);
				//$layername = $templatelayer[0]->layername;
				//$string = file_get_contents(base_url().'media/uploads/jsonfile/'.$template->jsonfile);
				$string = file_get_contents(base_url().'media/uploads/jsonfile/housepaint.json');
				$json_string = json_decode($string);
				$txt1 = $_POST['textname1'];
				$txt2 = $_POST['textname2'];
				$txt3 = $_POST['textname3'];
				$desc1 = $_POST['desc1'];
				$projname = $_POST['pname'];
				
				$json_string[0]->Text4 = $txt1;
				$json_string[0]->Text3 = $txt2;
				$json_string[0]->Text2 = $txt3;
				$json_string[0]->Text1 = $desc1;
				// Copy bg Image file to coresponding scene image
				if(!empty($_FILES['addimage1']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addimage1');
					$this->addimage1->initialize($config);
					if (!$this->addimage1->do_upload('addimage1')) 
					{
						print_r($this->addimage1->display_errors());
					}
					else
					{
						$filepath = $this->addimage1->data();
						$addimage1 = $filepath['file_name'];
					}
				}
				if(!empty($_FILES['addimage2']['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'jpg|png|jpeg|JPG|PNG|JPEG';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'addimage2');
					$this->addimage2->initialize($config);
					if (!$this->addimage2->do_upload('addimage2')) 
					{
						print_r($this->addimage2->display_errors());
					}
					else
					{
						$filepath = $this->addimage2->data();
						$addimage2 = $filepath['file_name'];
					}
				}
				if(!empty($addimage2))
				{
					$json_string[0]->placeholderimage2 = base_url().'media/uploads/footage/'.$addimage2;
				}
				if(!empty($addimage1))
				{
					$json_string[0]->placeholderimage1 = base_url().'media/uploads/footage/'.$addimage1;
				}
				$json_string[0]->ID = $projname;
				//echo $_POST['music_name_val'];exit;
				if(!empty($_POST['music_name_val']))
				{
					$musicName = $_POST['music_name_val'];
					$musicdst = APPPATH . "../media/uploads/audio_files/";
				}
				//print_r($json_string);exit;
				$json_sting_encode = json_encode($json_string);
				$dst = APPPATH . "../media/uploads/jsonfile";
				$dstaep = APPPATH . "../media/uploads/video_file";
				//exit();
				$fileLocation = $projname.".json";
				$file = fopen($dst."/".$fileLocation,"w+");
				fwrite($file,$json_sting_encode);
				//file_put_contents($file, $json_sting_encode);
				fclose($file); 
				//exit;
				$tstring = file_get_contents(base_url().'media/uploads/jsonfile/templater-options.json');
				$tjson_string = json_decode($tstring);
				//print_r($tjson_string);exit;
				//$template_name = $template->tname;
				$template_name = "House painter";
				//$template_aep = $template->tfname;
				$template_aep = "House painter.aep";
				
				$tjson_string->prefs->default_target = $template_name;
				$tjson_string->data_source = 'C:\\Templater\\input\\'.$fileLocation;
				$tjson_string->aep = 'C:\\Templater\\input\\'.$template_aep;
				$tjson_sting_encode = json_encode($tjson_string);
				$tfile = $dst.'/templater-options.json';
				file_put_contents($tfile, $tjson_sting_encode);
				$generatefname = $tjson_string->output_prefix.'_'.$tjson_string->prefs->default_target.'_'.$projname.'.avi';
				//print_r($tjson_string);exit;
				$ffmpegpath = "C:\\Templater\\ffmpeg\\bin\\ffmpeg.exe";
				$outfilename = $tjson_string->output_prefix.'_'.$projname.'.mp4';
				if($musicName != '')
				{
					$generateoutputwithaudio = $projname.'.mp4';
				}
				else
				{
					$generateoutputwithaudio = $outfilename;
				}
	
				$sting .= 'Copy-Item -Path '.$dst.'/'.$fileLocation.' -Destination C:\\Templater\\input\\'.$fileLocation. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dstaep.'/'.$template_aep.' -Destination C:\\Templater\\input\\'.$template_aep. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dst.'/templater-options.json -Destination C:\Templater\input\templater-options.json'. PHP_EOL;
				$sting .= 'C:\Templater\input\templater.ps1 -v "CC 2019"'. PHP_EOL;
				$sting .= $ffmpegpath.' -y -i C:\\Templater\\output\\'.$generatefname.' -c:v libx264 -c:a aac -pix_fmt yuv420p -movflags faststart -hide_banner C:\\Templater\\output\\'.$outfilename. PHP_EOL;
				//$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$outfilename.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$outfilename. PHP_EOL;
				if($musicName != '')
				{
				$sting .= 'Copy-Item -Path '.$musicdst.$musicName.' -Destination C:\\Templater\\input\\'.$musicName. PHP_EOL;
				$sting .= $ffmpegpath.' -i C:\\Templater\\output\\'.$outfilename.' -i C:\\Templater\input\\'.$musicName.' -map 0:v -map 1:a -c copy -shortest C:\\Templater\\output\\'.$generateoutputwithaudio. PHP_EOL;
				}
				$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$generateoutputwithaudio.' -Destination C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio. PHP_EOL;
				
				//echo $sting;exit;
				$dstp = APPPATH . "../media/uploads/powershellfile";
				$pfile = $dstp.'/temp.ps1';
				file_put_contents($pfile, $sting);
				Shell_Exec ('start /WAIT C:\Windows\WinSxS\wow64_microsoft-windows-powershell-exe_31bf3856ad364e35_10.0.14393.206_none_ad6ee618d45c7fca\powershell.exe  -command C:\xampp\htdocs\socialmediaads-v1\media\uploads\powershellfile\temp.ps1 2>&1 &');
				
				if (file_exists('C:\\xampp\\htdocs\\socialmediaads-v1\\media\\uploads\\output\\'.$generateoutputwithaudio))
				{
					$data_v = array(
					'userid'=>$userinfo,
					'tempid'=>$tid,
					'uvidname'=>$generateoutputwithaudio,
					'is_active'=>1,
					'is_delete'=>0,
					'created_date'=>date("Y-m-d H:i:s"),
					'updated_date'=>''
					);
					$this->user->form_insert('vc_project',$data_v);
				}
			}
			$data['projects'] = $this->user->where_fetch_data('vc_project', '', '', 'userid', $userinfo);
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/projects', $data);	
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	public function amvtemplate(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('ads_user_Id');
		if(isset($userinfo))
		{
			$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
			$this->load->view('user/amvtemplate',$data);
		}
		else
		{
			redirect('login', 'location');
		} 
	}
}