<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->session->sess_expiration = '86400';
		$this->load->model('admin');
	}
	public function index()
	{
		$this->load->library('session');
		$userinfo = $this->session->userdata('ads_logged_in');
		$userid = $this->session->userdata('ads_userId');
		//$data['users'] = $this->admin->get_user_data();
		
		if(isset($userinfo)){
		
			$data['user'] = $this->admin->GetAllData('vc_user');
			$data['tot_user'] = count($data['user']);
			$active_user = 0;
			$inactive_user = 0;
			$delete_user = 0;
			$user_array = array('');
			foreach($data['user'] as $res_user)
			{
				array_push($user_array, $res_user->id);
				if(($res_user->is_active == '1') && ($res_user->is_delete == '0'))
				{
					$active_user++;
				}
				if(($res_user->is_active == '0') && ($res_user->is_delete == '0'))
				{
					$inactive_user++;
				}
				if($res_user->is_delete == '1')
				{
					$delete_user++;
				}
			}
			array_shift($user_array);
			$data['user_array'] = $user_array;
			$data['active_user'] = $active_user;
			$data['inactive_user'] = $inactive_user;
			$data['delete_user'] = $delete_user;
			$data['templates'] = $this->admin->get_all_data('vc_videotemplate');
			$data['tot_templates'] = count($data['templates']);
			$data['accesslevel'] = $this->admin->get_all_data('vc_accesslevel');
			$level1_template =0;
			$level2_template =0;
			$level3_template =0;
			$level4_template =0;
			foreach($data['accesslevel'] as $accesslevel)
			{
				if($accesslevel->level_id == '1')
				{
					$level1_template++;
				}
				if($accesslevel->level_id == '2')
				{
					$level2_template++;
				}
				if($accesslevel->level_id == '3')
				{
					$level3_template++;
				}
				if($accesslevel->level_id == '4')
				{
					$level4_template++;
				}
			}
			$data['level1_template'] = $level1_template;
			$data['level2_template'] = $level2_template;
			$data['level3_template'] = $level3_template;
			$data['level4_template'] = $level4_template;
			$data['projects'] = $this->admin->get_all_data('vc_project');
			$user_count = count($user_array);
			$array_project =array('');
			for ($i = 0; $i < $user_count; $i++) 
			{
				$data['checkproject_count'] =$this->admin->getRecordByFildName('vc_project','userid',$user_array[$i]);
				array_push($array_project, count($data['checkproject_count']));
			}
			array_shift($array_project);
			$data['array_project'] = $array_project;
			$this->load->view('admin/dashboard/head');
			$this->load->view('admin/dashboard/sidebar');
			$this->load->view('admin/dashboard/dashboard');
			$this->load->view('admin/dashboard/footer');
		}
		else
		{
			redirect('admin', 'location');
		}
	}	
	
	public function is_login()
	{
		if($this->input->post()){
			if(isset($_POST['submit'])){
				$username = $this->input->post('username');
			    $password = $this->input->post('password');
				$result = $this->admin->login_check($username,$password);
				//echo "<pre>"; print_r($result); die;
				if(!empty($result)){
				$sess_array = array(
					'id' => $result[0]->id,
					'first_name' => $result[0]->first_name,
					'last_name' => $result[0]->last_name,
					'email' =>$result[0]->email 
					);  
				$datetime = date('Y-m-d H:i:s');
				$tablename ="fj_admin";
				$id = $result[0]->id;
				$data = array('last_login'=>$datetime);
				$this->admin->form_update($tablename,$data,$id);
				//echo "<pre>"; print_r($sess_array); die;
				// Add user data in session
				$this->session->set_userdata('ads_logged_in',$sess_array);
				$this->session->set_userdata('ads_userId',$result[0]->id);
				$this->session->set_flashdata('success_msg', 'Login successfully');
				//$this->load->view('admin/dashboard');
				redirect('admin/dashboard');
				}else{
				$this->session->set_flashdata('error_msg', 'username/password not correct');	
				}
			}else if(isset($_POST['submit'])&& $_POST['submit']=='forgot'){
				redirect('admin-forget-password', 'location');
			}
		}
		$this->load->view('admin/login/login');	
		//$this->load->view('admin/login/login-4');
	}
	public function forgottenpassword(){
		$result = $this->admin->user_email_check($_POST['email']);
		//$result[0]->user_email, $result[0]->uname
		if(!empty($result)){
		$this->email->from('info@converthink.in', 'Converthink Team');
		$this->email->to('sisirbehura@gmail.com');
		$this->email->subject('Photo Proffessional User Password Reset');
		$body = '
		Hi Sisir,
		You recently requested to reset your password for your Photo Proffessional Account.
		Click the button below to reset it.
		
		<a href="'.base_url().'admin/login/resetpassword" target="_blank">Reset Your Password</a>
		
		if you did not request a password reset, please ignore this email or reply to let us know.
		this password reset is only valid for the next 30 minutes.
		
		Thanks,
		Converthink Team';
		$this->email->message($body);
		$this->email->send();
		redirect('login', 'location');
		//exit;
		}
	}
	public function forgetpassword(){
		$this->load->view('admin/login/forgetpassword');	
	}
	function resetpassword()
	{
		$this->load->view('admin/login/resetpassword');
	}
	function updatepass()
	{
		$new_pass = $this->input->post('new_pass');
		$con_pass = $this->input->post('con_pass');
		$user_id = '1';
		$form_array = array(
		'password' => md5($new_pass)
		); 
		$this->admin->form_update('vc_admin',$form_array,$user_id);
		redirect('admin', 'location');
	}
	function signout()
	{
		$this->session->sess_destroy();
		redirect('admin', 'location');
	}
}
